﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewtonLab
{
    class GameObjectLibrary
    {

        /// <summary>
        /// A library containing all creatable game objects.
        /// </summary>
        private Dictionary<string, GameObject> Library = new Dictionary<string, GameObject>();

        /// <summary>
        /// Returns true if the library contains an entry with specified name.
        /// </summary>
        /// <param name="name">Name of an entry.</param>
        /// <returns>Returns true if the library contains an entry with specified name.</returns>
        public bool Contains(string name)
        {
            return Library.ContainsKey(name);
        }

        /// <summary>
        /// Get a specific entry with specified name.
        /// </summary>
        /// <param name="name">Name of an entry.</param>
        /// <returns>Returns an entry with specified name, null of does not have item.</returns>
        public GameObject Get(string name)
        {
            return Library[name];
        }

        /// <summary>
        /// Add an entry to the library.
        /// </summary>
        /// <param name="name">Name of the entry to add.</param>
        /// <param name="gameObject">The game object of the entry to add.</param>
        public void Add(string name, GameObject gameObject) 
        {
            if (!Contains(name))
            {
                Library.Add(name, gameObject);
            }
        }
    }
}
