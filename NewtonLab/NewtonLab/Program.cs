﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

/* SharpDX Libraries */
using SharpDX;
using SharpDX.Windows;  /* RenderForm */
using SharpDX.Direct3D;
using SharpDX.Direct3D11;
using SharpDX.DXGI;
using FactoryDXGI = SharpDX.DXGI.Factory1;

namespace NewtonLab
{
    class Program
    {
        /// <summary>
        /// Entry Point.
        /// </summary>
        /// <param name="args">Command line arguments</param>
        static void Main(string[] args)
        {
            Game game = new Game();
            game.Start();
        }
    }
}
