﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Globalization;

using SharpDX;

namespace NewtonLab.Graphics.ObjImport
{
    class ObjReader
    {
        private CultureInfo infos = CultureInfo.InvariantCulture;

        /// <summary>
        /// Creates and returns a Model object from a .obj file.
        /// </summary>
        /// <param name="filepath">The path of the obj file to use to create a Model object. </param>
        /// <returns> Returns a Model object created from specified .obj file. </returns>
        public List<Geometry> CreateGeoms(string filepath)
        {
            /* Create Geometry that will be used to create the model */
            List<Geometry> geom = new List<Geometry>();

            List<string> lines;    /* Lines read and to be parsed */
            List<Vector3> positions = new List<Vector3>();
            List<Vector3> normals = new List<Vector3>();
            List<Vector2> textures = new List<Vector2>();
            List<Material> materials = new List<Material>();
            List<int> faces = new List<int>();

            lines = ReadAllLines(filepath);

            /* Parse each line based on it's label, store appropriately */
            foreach (string line in lines)
            {
                if (line.Contains("v "))        /* Read a vertex */
                {
                    positions.Add(GetPosition(line));
                }
                else if (line.Contains("vn "))  /* Read a normal */
                {
                    normals.Add(GetNormal(line));
                }
                else if (line.Contains("vt "))  /* Read a Texture */
                {
                    textures.Add(GetTexture(line));
                }
                else if (line.Contains("mtllib "))  /* Read a material */
                {
                    string path = System.IO.Path.GetDirectoryName(filepath) + "\\" + line.Replace("mtllib", "").Trim();
                    materials.AddRange(LoadMaterial(path));
                }
            }

            string currentMesh = "default";
            Material currentMate = new Material();
            string lastMtlLine = "";

            foreach (string line in lines)
            {
                if (line.Contains("f "))
                {
                    faces.AddRange(GetFace(line));
                }
                else if (line.Contains("usemtl ") && line.CompareTo(lastMtlLine) != 0)
                {
                    if (faces.Count > 0)
                        geom.Add(CreateGeom(currentMesh, positions, normals, textures, faces, currentMate));

                    IEnumerable<Material> materialsFound = (from m in materials where m.MaterialName == line.Replace("usemtl", "").Trim() select m);
                    currentMate = materialsFound.First();
                    faces.Clear();
                    lastMtlLine = String.Copy(line);
                }
                else if (line.Contains("g "))
                {
                    if (faces.Count > 0)
                        geom.Add(CreateGeom(currentMesh, positions, normals, textures, faces, currentMate));

                    currentMesh = line.Replace("g", "").Trim();

                    faces.Clear();
                }
                else if (line.Contains("# "))
                {
                    //commento
                }
            }


            if (faces.Count > 0)
                geom.Add(CreateGeom(currentMesh, positions, normals, textures, faces, currentMate));

            return geom;
        }

        /// <summary>
        /// Reads all the lines of an obj file into a list of strings. Filters out comments (#).
        /// </summary>
        /// <param name="filepath">The filepath of the obj file to be read. </param>
        /// <returns> Lines of the input obj file as List of strings. </returns>
        private List<string> ReadAllLines(string filepath)
        {
            List<string> lines = new List<string>();

            /* Read file until end of file, stores lines, skip lines starting with # */
            using (StreamReader reader = new StreamReader(filepath))
            {
                for (; ; )
                {
                    string l = reader.ReadLine();
                    if (reader.EndOfStream)
                        break;

                    if (l.Contains("#") || string.IsNullOrEmpty(l.Trim()))
                        continue;

                    lines.Add(l);
                }
            }

            return lines;
        }

        private Vector3 GetPosition(string line)
        {
            string[] parts = Parts("v", line);
            return new Vector3(
                float.Parse(parts[0], infos),
                float.Parse(parts[1], infos),
                float.Parse(parts[2], infos));
        }

        private Vector3 GetNormal(string line)
        {
            string[] parts = Parts("vn", line);
            return new Vector3(
                float.Parse(parts[0], infos),
                float.Parse(parts[1], infos),
                float.Parse(parts[2], infos));
        }

        private Vector2 GetTexture(string line)
        {
            string[] parts = Parts("vt", line);
            return new Vector2(
                float.Parse(parts[0], infos),
               1 - float.Parse(parts[1], infos));
        }

        /// <summary>
        /// Parses the input lines to read faces and store into a list. 
        /// </summary>
        /// <param name="line">The lines to be parsed. </param>
        /// <returns> An array of ints representing the faces. </returns>
        private List<int> GetFace(string line)
        {
            string[] parts = Parts("f", line);
            List<int> lists = new List<int>();
            foreach (String s in parts)
            {
                lists.AddRange((from ss in s.Split('/') select int.Parse(ss)).ToArray());
            }
            return lists;
        }

        /// <summary>
        /// Creates and returns material object array from specified file.
        /// </summary>
        /// <param name="filepath">The filepath of the input file. </param>
        /// <returns> Array of Material created from input file. </returns>
        private Material[] LoadMaterial(String filepath)
        {
            Material current = null;
            List<Material> materials = new List<Material>();
            using (StreamReader reader = new StreamReader(filepath))
            {


                for (; ; )
                {
                    if (reader.EndOfStream)
                        break;
                    string line = reader.ReadLine();



                    if (line.Contains("newmtl"))
                    {
                        if (current != null)
                            materials.Add(current);

                        current = new Material();
                        current.MaterialName = line.Replace("newmtl", "").Trim();
                    }
                    else if (line.Contains("map_Kd"))
                    {
                        current.DiffuseMap = line.Replace("map_Kd", "").Trim();
                    }
                    else if (line.Contains("map_Ka"))
                    {

                    }
                    else if (line.Contains("Ka"))
                    {
                        float[] val = (from s in line.Replace("Ka", "").Trim().Split(' ') select float.Parse(s, infos)).ToArray();
                        current.Ambient = new Vector3(val[0], val[1], val[2]);
                    }
                    else if (line.Contains("Kd"))
                    {
                        float[] val = (from s in line.Replace("Kd", "").Trim().Split(' ') select float.Parse(s, infos)).ToArray();
                        current.Diffuse = new Vector3(val[0], val[1], val[2]);
                    }
                    else if (line.Contains("Ks"))
                    {
                        float[] val = (from s in line.Replace("Ks", "").Trim().Split(' ') select float.Parse(s, infos)).ToArray();
                        current.Specular = new Vector3(val[0], val[1], val[2]);
                    }
                    else if (line.Contains("Ns"))
                    {
                        float[] val = (from s in line.Replace("Ns", "").Trim().Split(' ') select float.Parse(s, infos)).ToArray();
                        current.Shininess = val.First();
                    }
                }
            }

            if (current != null)
                materials.Add(current);

            return materials.ToArray();
        }

        /// <summary>
        /// Splits a line into a string array of words/values. Removes specified string from line.
        /// </summary>
        /// <param name="name">The string to be removed from the line. </param>
        /// <param name="line">The line to be parsed. </param>
        /// <returns> A string array of "words" without input string parameter name. </returns>
        private string[] Parts(string name, string line)
        {
            return line.Replace(name, "").Trim().Split(' ');
        }

        private Geometry CreateGeom(string name, List<Vector3> position, List<Vector3> normals, List<Vector2> texture, List<int> faces, Material mate)
        {
            Geometry geom = new Geometry();
            geom.Name = name;
            geom.VertexData = new List<StaticVertex>();
            geom.IndexData = new List<int>();

            int stride = 0;


            if (position.Count > 0)
                stride++;


            if (normals.Count > 0)
                stride++;


            if (texture.Count > 0)
                stride++;


            int vertexCount = faces.Count / stride;

            int k = 0;


            for (int i = 0; i < vertexCount; i++)
            {
                StaticVertex v = new StaticVertex();
                if (position.Count > 0)
                {
                    v.Position = position[faces[k] - 1];
                    k++;
                }

                if (texture.Count > 0)
                {
                    v.TextureCoordinate = texture[faces[k] - 1];
                    k++;
                }

                if (normals.Count > 0)
                {
                    v.Normal = normals[faces[k] - 1];
                    k++;
                }
                geom.VertexData.Add(v);

            }

            for (int i = 0; i < (geom.VertexData.Count); i++)
            {
                geom.IndexData.Add((short)i);
            }

            geom.MeshMaterial.Add(mate);
            geom.FaceCounts.Add(geom.IndexData.Count);

            //geom.Optimize();

            return geom;

        }
    }
}
