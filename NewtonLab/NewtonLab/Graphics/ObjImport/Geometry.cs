﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpDX;

namespace NewtonLab.Graphics.ObjImport
{
    class Geometry
    {
        public string Name { get; set; }
        public List<StaticVertex> VertexData { get; set; }
        public List<int> IndexData { get; set; }
        public List<Material> MeshMaterial { get; set; }
        public List<int> FaceCounts { get; set; }

        public Geometry()
        {
            this.IndexData = new List<int>();
            this.Name = "";
            this.VertexData = new List<StaticVertex>();
            this.MeshMaterial = new List<Material>();
            this.FaceCounts = new List<int>();
        }

        /// <summary>
        /// Optimize model
        /// </summary>
        public void Optimize()
        {
            GenerateIndices();
            GenerateNormal();
        }


        private void GenerateNormal()
        {

            for (int i = 0; i < VertexData.Count; i++)
            {
                StaticVertex v = VertexData[i];
                v.Normal = new Vector3();
                VertexData[i] = v;
            }

            for (int i = 0; i < IndexData.Count; i += 3)
            {

                StaticVertex p1 = VertexData[IndexData[i]];
                StaticVertex p2 = VertexData[IndexData[i + 1]];
                StaticVertex p3 = VertexData[IndexData[i + 2]];

                Vector3 V1 = p2.Position - p1.Position;
                Vector3 V2 = p3.Position - p1.Position;

                Vector3 N = Vector3.Cross(V1, V2);
                N.Normalize();

                p1.Normal += N;
                p2.Normal += N;
                p3.Normal += N;

                VertexData[IndexData[i]] = p1;
                VertexData[IndexData[i + 1]] = p2;
                VertexData[IndexData[i + 2]] = p3;
            }

            //normalize
            for (int i = 0; i < VertexData.Count; i++)
            {
                StaticVertex v = VertexData[i];
                v.Normal.Normalize();
                VertexData[i] = v;
            }

        }

        private void GenerateIndices()
        {
            List<StaticVertex> tempVertices = new List<StaticVertex>();
            List<int> tempIndices = new List<int>();

            foreach (StaticVertex v in VertexData)
            {
                //verifica se esiste un vertice già nella lista
                int i = 0;
                bool found = false;
                foreach (StaticVertex v2 in tempVertices)
                {
                    if (StaticVertex.Compare(v, v2))
                    {
                        //travato
                        found = true;
                        break;
                    }
                    i++;
                }

                if (found)
                {
                    tempIndices.Add(i);
                    StaticVertex v2 = tempVertices[i];
                    //somma le normali
                }
                else
                {
                    i = tempVertices.Count;
                    tempVertices.Add(v);
                    tempIndices.Add(i);
                }

                //normali
                StaticVertex vTemp = tempVertices[i];
                vTemp.Normal += v.Normal;
                tempVertices[i] = vTemp;
            }


            //normalizzazione finale
            VertexData.Clear();
            //foreach (VertexFormat v in tempVertices)
            //{
            //    v.Normal.Normalize();
            //    Vertices.Add(v);
            //}
            VertexData.AddRange(tempVertices);

            IndexData.Clear();
            IndexData.AddRange(tempIndices);

        }
    }
}
