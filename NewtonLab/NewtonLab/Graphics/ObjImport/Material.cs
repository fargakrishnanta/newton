﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpDX;

namespace NewtonLab.Graphics.ObjImport
{
    /// <summary>
    /// The Material of the graphics object.
    /// </summary>
    public class Material
    {
        /// <summary>
        /// Name
        /// </summary>
        public string MaterialName;
        /// <summary>
        /// Ambient color
        /// </summary>
        public Vector3 Ambient;
        /// <summary>
        /// Diffuse color
        /// </summary>
        public Vector3 Diffuse;
        /// <summary>
        /// Specular color
        /// </summary>
        public Vector3 Specular;
        /// <summary>
        /// Shininess
        /// </summary>
        public float Shininess;
        /// <summary>
        /// Texture name
        /// </summary>
        public string DiffuseMap;

        /// <summary>
        /// Normal Texture name
        /// </summary>
        public string NormalMap;
    }
}
