﻿using SharpDX;
using SharpDX.DirectInput;
using SharpDX.Direct3D11;
using SharpDX.IO;
using SharpDX.Windows;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows.Forms;

using Color11 = SharpDX.Color;

namespace NewtonLab.Graphics.GUI
{

    class TextBox : GUIObject
    {
        public Vector2 size { get; private set; }

        protected ShaderResourceView TextureClicked { get; private set; }
        public ShaderResourceView OriTexture { get; set; }


        public Color11 Color { get; set; }

        public float txtScale { get; set; }

        public TextBox(Vector2 Position, Color11 Color, string text = null, string filename = null, string filenameClicked = null, bool selectable = true, RenderForm renderForm = null, GraphicsHandler gHandler = null)
            : base(filename, renderForm, gHandler)
        {
            this.Position = Position;
            this.Editable = selectable;
            Text = text;
           
            if(filename != null && selectable)
                TextureClicked = ShaderResourceView.FromFile(this.GraphicsHandler.D3DHandler.Device, filenameClicked);

            this.Color = Color;
            txtScale = 0.6f;
            OriTexture = Texture;

        }
        
        /// <summary>
        /// Draw texture and what
        /// </summary>
        public override void Draw()
        {
            if (Texture != null)
            {
                size = GetTextureSize(Texture);
                Batch.Draw(Texture, new RectangleF(Position.X - 5, Position.Y - 5, size.X, size.Y), Color11.White);
                
            }
            if (Text != null)
            {
                FontRenderer.DrawString(Text, (int)Position.X, (int)Position.Y, Color, txtScale);
            }
        }

        /// <summary>
        /// Update texture when clicked and unclicked
        /// </summary>
        public override void UpdateTexture() 
        {
            Texture = OriTexture;
        }

        /// <summary>
        /// Handle inputs 
        /// </summary>
        /// <param name="pointClicked">The point that was clicked on the screen.</param>
        /// <returns>Returns true if clicked, false if not.</returns>
        public override bool HandleInputs(Vector2 pointClicked)
        {
            
            if (pointClicked.X > Position.X && pointClicked.X < (Position.X + size.X))
            {
                if ((pointClicked.Y > Position.Y) && (pointClicked.Y < (Position.Y + size.Y)))
                {
                    if (this.Editable)
                    {
                        if (TextureClicked != null)
                            Texture = TextureClicked;
                        return true;
                    }
                        
                    return true;
                }
            }
               
            return false;
        }
    }
}
