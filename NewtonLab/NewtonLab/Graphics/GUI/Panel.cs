﻿using SharpDX;
using SharpDX.Toolkit.Graphics;
using SharpDX.Direct3D11;
using SharpDX.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Color11 = SharpDX.Color;
namespace NewtonLab.Graphics.GUI
{
    class Panel
    {
        public List<GUIObject> GuiObjects = new List<GUIObject>();
        public bool isShow {get; set;}
        
        public GUIObject selectedGUI { get; private set; }

        public GraphicsHandler GraphicsHandler { get; set; }
        public Vector2 size { get; private set; }
        protected SpriteBatch Batch { get; private set; }

        protected ShaderResourceView Texture { get; set; }

        public Vector2 Position { get; set; }
        public Panel(Vector2 Position, GraphicsHandler gHandler, string filename = null)
        {
            gHandler.GUIRenderer.AddPanel(this);
            this.Position = Position;
            Batch = gHandler.GUIRenderer.Batch;
            if (filename != null)
            {
                
                Texture = ShaderResourceView.FromFile(gHandler.D3DHandler.Device, filename);
            }
            this.isShow = true;
        }

        /// <summary>
        /// Add an object to the panel.
        /// </summary>
        /// <param name="gui">The GUIObject to add to the panel.</param>
        public void AddObject(GUIObject gui)
        {
           
            GuiObjects.Add(gui);
        }
        protected Vector2 GetTextureSize(ShaderResourceView tex)
        {
            using (var resource = tex.Resource)
            {
                using (var texture2D = resource.QueryInterface<SharpDX.Direct3D11.Texture2D>())
                    return new Vector2(texture2D.Description.Width, texture2D.Description.Height);
            }
        }
        /// <summary>
        /// Draw all objects in this panel.
        /// </summary>
        public void DrawAll(){
        
            if(Texture !=null)
            {
                size = GetTextureSize(Texture);
                Batch.Draw(Texture, new RectangleF(Position.X - 5, Position.Y - 5, size.X, size.Y), Color11.White);
            }
            
            foreach (GUIObject gui in GuiObjects)
            {        
                if(gui.isShow)
                    gui.Draw();  
            }
        }

        /// <summary>
        /// Detects and handles clicks on GUI elements
        /// </summary>
        /// <param name="pointClicked">The screen position when clicked. </param>
        /// <returns> Returns true if a GUI element is clicked, false otherwise. </returns>
        public bool HandleInput(Vector2 pointClicked)
        {
            foreach (GUIObject guiObject in GuiObjects)
            {
                if (guiObject.HandleInputs(pointClicked))
                {
                    selectedGUI = guiObject;
                    return true;
                }
            }

            if (pointClicked.X > Position.X && pointClicked.X < (Position.X + size.X))
            {
                if ((pointClicked.Y > Position.Y) && (pointClicked.Y < (Position.Y + size.Y)))
                {
                    selectedGUI = null;
                    return true;
                }
            }

            return false;
        }

        
        /// <summary>
        /// Show this GUI Object.
        /// </summary>
        public void Show()
        {
            this.isShow = true;
            
            /* Show all Child Objects */
            foreach (GUIObject go in GuiObjects)
            {
                go.Show();
            }
        }

        /// <summary>
        /// Hide this GUI Object.
        /// </summary>
        public void Hide()
        {
            this.isShow = false;
        }
    }
}
