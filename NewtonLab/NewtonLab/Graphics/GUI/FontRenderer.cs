﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;
using SharpDX;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit.Content;
using SharpDX.Toolkit;
namespace NewtonLab.Graphics
{
    class FontRenderer
    {
        /// <summary>
        /// Graphics device pointer
        /// </summary>
        public GraphicsDevice GraphicsDevice { get; private set; }

        /// <summary>
        /// Device reference
        /// </summary>
        public D3DHandler D3DHandler { get; private set; }

        /// <summary>
        /// Main font for drawing text
        /// </summary>
        public SpriteFont Font { get; private set; }

        /// <summary>
        /// Batch
        /// </summary>
        public SpriteBatch Batch { get; set; }
        public ContentManager content { get; set; }

      
        public FontRenderer(SpriteBatch spriteBatch, GraphicsDevice gDevice, string filename)
        {
            
            
            Batch = spriteBatch;
            GraphicsDevice = gDevice;
            
            Font = SpriteFont.Load(GraphicsDevice, filename);
            
            
        }

        /// <summary>
        /// Draw text
        /// </summary>
        /// <param name="text">Text</param>
        /// <param name="x">Left position</param>
        /// <param name="y">Top position</param>
        /// <param name="color">Text color</param>
        /// <param name="scale">The Scale of the draw string</param>
        public void DrawString(string text, int x, int y, Color color, float scale = 0.5f)
        {
            //Batch.DrawString(Font, text, new Vector2(x, y), color);
            Batch.DrawString(Font, text, new Vector2(x, y), color, 0.0f, new Vector2(0,0), scale, SpriteEffects.None, 1.0f);
        }

    }
}
