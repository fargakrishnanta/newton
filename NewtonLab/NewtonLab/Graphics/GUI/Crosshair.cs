﻿using SharpDX;
using SharpDX.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Color11 = SharpDX.Color;
using Point = System.Drawing.Point;

namespace NewtonLab.Graphics.GUI
{
    class Crosshair : GUIObject
    {
        Vector2 size;
        float sizePercentage = 0;

        public Crosshair(string filename, float sizePercentage, RenderForm renderForm = null, GraphicsHandler gHandler = null)
            : base(filename, renderForm, gHandler)
        {
            this.sizePercentage = sizePercentage;
        }

        /// <summary>
        /// Does not apply for the crosshair, would throw exception.
        /// </summary>
        public override void UpdateTexture()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Draw the GUI Object.
        /// </summary>
        public override void Draw()
        {
            if (Texture != null)
            {
                size = GetTextureSize(Texture);
                Point centerPoint = new Point(RenderForm.ClientSize.Width / 2, RenderForm.ClientSize.Height / 2);
                Batch.Draw(Texture, new RectangleF(centerPoint.X - size.X * sizePercentage / 2, centerPoint.Y - size.Y * sizePercentage / 2,
                    size.X * sizePercentage, size.Y * sizePercentage), Color11.White);

            }
        }

        /// <summary>
        /// Does not apply for the crosshair.
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        public override bool HandleInputs(Vector2 position)
        {
            return false;
        }
    }
}
