﻿

using SharpDX;
using SharpDX.DirectInput;
using SharpDX.Direct3D11;
using SharpDX.IO;
using SharpDX.Windows;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Color11 = SharpDX.Color;
namespace NewtonLab.Graphics.GUI
{

   
    class Switch : GUIObject
    {

        public bool isOn { get; set; }

        public Vector2 size { get; private set; }

        protected ShaderResourceView TextureClicked { get; private set; }
        public ShaderResourceView OriTexture { get; private set; }

        public Switch(Vector2 Position, string text = null, string filename = null, bool on = false, RenderForm renderForm = null, GraphicsHandler gHandler = null)
            : base(filename, renderForm, gHandler)
        {
            this.Position = Position;

            if(filename != null){
                TextureClicked = ShaderResourceView.FromFile(this.GraphicsHandler.D3DHandler.Device, "./Sprites/switch_on.gif");
                OriTexture = Texture;
                isOn = true;
            }

            if (on == false)
            {
                isOn = false;
            }

           
        }

        /// <summary>
        /// Draw this GUI Object.
        /// </summary>
        public override void Draw()
        {
            if (Texture != null)
            {
                size = GetTextureSize(Texture);
                Batch.Draw(Texture, new RectangleF(Position.X - 5, Position.Y - 5, size.X, size.Y), Color11.White);
            }
        }

        /// <summary>
        /// Show the default Texture.
        /// </summary>
        public override void UpdateTexture()
        {
            Texture = OriTexture;
        }

        /// <summary>
        /// Show the On Texture.
        /// </summary>
        public void on()
        {
            Texture = TextureClicked;
            isOn = true;
        }

        /// <summary>
        /// Show the Off Texture.
        /// </summary>
        public void off()
        {
            Texture = OriTexture;
            isOn = false;
        }

        /// <summary>
        /// Returns true when the GUI Object is clicked.
        /// </summary>
        /// <param name="pointClicked">The screen coordinate that has been clicked.</param>
        /// <returns>Returns true when the GUI Object is clicked.</returns>
        public override bool HandleInputs(SharpDX.Vector2 pointClicked)
        {
            if (pointClicked.X > Position.X && pointClicked.X < (Position.X + size.X))
            {
                if ((pointClicked.Y > Position.Y) && (pointClicked.Y < (Position.Y + size.Y)))
                {
                   
                    
                    return true;
                }
            }

            return false;
        }
    }
}
