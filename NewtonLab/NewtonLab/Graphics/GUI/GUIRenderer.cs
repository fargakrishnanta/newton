﻿using SharpDX;
using SharpDX.Direct3D11;
using SharpDX.Toolkit.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Color11 = SharpDX.Color;

namespace NewtonLab.Graphics.GUI
{
    class GUIRenderer
    {
        /// <summary>
        ///  List of Panels to draw.
        /// </summary>
        private List<Panel> Panels = new List<Panel>();
        public D3DHandler D3DHandler { get; private set; }
        public FontRenderer FontRenderer { get; private set; }

        public Panel selectedPanel { get; private set; }
        /// <summary>
        /// Graphics device pointer
        /// </summary>
        public GraphicsDevice GraphicsDevice { get; private set; }
        /// <summary>
        /// Batch
        /// </summary>
        public SpriteBatch Batch { get; set; }
        public PrimitiveBatch<VertexPositionColor> PrimitiveBatch { get; private set; }
        public static VectorPainter VectorPainter { get; private set; }
        private BasicEffect basicEffect;

        public GUIRenderer(GraphicsHandler gHandler)
        {
            /* Set Defaults */
            GUIObject.DefaultGraphicsHandler = gHandler;

            /* Store D3DHandler */
            D3DHandler = gHandler.D3DHandler;

            /* Initialize the graphics device */
            GraphicsDevice = GraphicsDevice.New(D3DHandler.Device);
            GraphicsDevice.SetViewports(D3DHandler.DeviceContext.Rasterizer.GetViewports()[0]);
            Batch = new SpriteBatch(GraphicsDevice);

            /* Initialize Font Renderer */
            FontRenderer = new FontRenderer(Batch, GraphicsDevice, "./Fonts/textfont.dds");

            /* Set up Primitive Batch */
            PrimitiveBatch = new PrimitiveBatch<VertexPositionColor>(GraphicsDevice);

            /* Set up Basic Effect for drawing using primitive batch */
            basicEffect = new BasicEffect(GraphicsDevice);
            basicEffect.Alpha = 1.0f;
            basicEffect.VertexColorEnabled = true;
            basicEffect.TextureEnabled = false;
            basicEffect.LightingEnabled = false;
            basicEffect.DiffuseColor = new Vector4(1.0f, 1.0f, 1.0f, 1.0f);
            basicEffect.VertexColorEnabled = true;
            basicEffect.Projection = gHandler.Projection;
        }

        /// <summary>
        /// Creates an instance of Vector Painter and returns it.
        /// </summary>
        /// <param name="camera">The camera for which the vectors will be drawn.</param>
        /// <returns>Returns an instance of Vector Painter.</returns>
        public static VectorPainter CreateVectorPainter(Camera camera)
        {
            return VectorPainter = new VectorPainter(camera);
        }

        /// <summary>
        /// Draws all gui elements.
        /// </summary>
        public void Frame()
        {
            /* GUI Drawing Starts here */
            Begin();

            DrawAll();
           
            End();

            DrawAxisVectors();
        }

        /// <summary>
        /// Begin a 2D drawing session
        /// </summary>
        public void Begin()
        {
            BlendStateDescription blendStateDescription = new BlendStateDescription();
            blendStateDescription.AlphaToCoverageEnable = false;

            blendStateDescription.RenderTarget[0].IsBlendEnabled = true;
            blendStateDescription.RenderTarget[0].SourceBlend = BlendOption.SourceAlpha;
            blendStateDescription.RenderTarget[0].DestinationBlend = BlendOption.InverseSourceAlpha;
            blendStateDescription.RenderTarget[0].BlendOperation = BlendOperation.Add;
            blendStateDescription.RenderTarget[0].SourceAlphaBlend = BlendOption.Zero;
            blendStateDescription.RenderTarget[0].DestinationAlphaBlend = BlendOption.Zero;
            blendStateDescription.RenderTarget[0].AlphaBlendOperation = BlendOperation.Add;
            blendStateDescription.RenderTarget[0].RenderTargetWriteMask = ColorWriteMaskFlags.All;

            Batch.Begin(SpriteSortMode.Immediate, SharpDX.Toolkit.Graphics.BlendState.New(GraphicsDevice, blendStateDescription));

        }

        /// <summary>
        /// End drawing session
        /// </summary>
        public void End()
        {
            Batch.End();
        }

        /// <summary>
        /// Add a Panel to be drawn.
        /// </summary>
        /// <param name="panel">The panel to add to the drawing list for rendering each loop.</param>
        public void AddPanel(Panel panel)
        {
            Panels.Add(panel);
        }

        /// <summary>
        /// Resize graphics device
        /// </summary>
        private void Resize()
        {
            GraphicsDevice.SetViewports(D3DHandler.DeviceContext.Rasterizer.GetViewports()[0]);
        }

        /// <summary>
        /// Draw each panel.
        /// </summary>
        private void DrawAll()
        {
            foreach (Panel panel in Panels)
            {
                if(panel.isShow)
                    panel.DrawAll();
            }
        }

        private void DrawAxisVectors()
        {
            basicEffect.CurrentTechnique.Passes[0].Apply();

            PrimitiveBatch.Begin();

            VectorPainter.Draw();

            PrimitiveBatch.End();
        }

        /// <summary>
        /// Detects and handles clicks on GUI elements
        /// </summary>
        /// <param name="pointClicked">The screen position when clicked. </param>
        /// <returns> Returns true if a GUI element is clicked, false otherwise. </returns>
        public bool HandleInputs(Vector2 pointClicked)
        {
            foreach (Panel panel in Panels)
            {
                if (panel.HandleInput(pointClicked))
                {
                    selectedPanel = panel;
                    return true;
                }
            }

            return false; /* Nothing was clicked */
        }
    }
}
