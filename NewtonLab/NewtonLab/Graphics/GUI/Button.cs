﻿using System;
using SharpDX;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Color11 = SharpDX.Color;
using SharpDX.Windows;
using SharpDX.Direct3D11;

namespace NewtonLab.Graphics.GUI
{
    class Button : GUIObject
    {
        
        public Vector2 size { get; private set; }
        protected ShaderResourceView TextureClicked { get; private set; }
        protected ShaderResourceView OriTexture { get; private set; }
        public Button(Vector2 Position, string text = null, string filename = null, string filenameClicked = null, RenderForm renderForm = null, GraphicsHandler gHandler = null)
            : base(filename, renderForm, gHandler)
        {

            this.Position = Position;
            
            Text = text;
            if (filename != null)
            {
                if(filenameClicked != null)
                    TextureClicked = ShaderResourceView.FromFile(this.GraphicsHandler.D3DHandler.Device, filenameClicked);
                OriTexture = Texture;
            }
           
        }

        /// <summary>
        /// Switches to the texture of when the button has been pressed.
        /// </summary>
        public void on()
        {
            Texture = TextureClicked;
        }

        /// <summary>
        /// Switches to the texture of when the button is not pressed.
        /// </summary>
        public void off()
        {
            Texture = OriTexture;
        }

        /// <summary>
        /// Draws the GUI Object.
        /// </summary>
        public override void Draw()
        {
            if (Texture != null)
            {
                size = GetTextureSize(Texture);

                Batch.Draw(Texture, new RectangleF(Position.X, Position.Y, size.X, size.Y), Color11.White);
            }
            if (Text != null)
            {
                FontRenderer.DrawString(Text, (int)Position.X, (int)Position.Y, Color11.White);
            }
        }

        /// <summary>
        /// Show the default texture.
        /// </summary>
        public override void UpdateTexture()
        {
            Texture = OriTexture;         
        }

        /// <summary>
        /// Returns true if the GUI object has been clicked.
        /// </summary>
        /// <param name="pointClicked">The point clicked on the screen.</param>
        /// <returns>Returns true if the GUI object has been clicked.</returns>
        public override bool HandleInputs(Vector2 pointClicked)
        {
            if (pointClicked.X > Position.X && pointClicked.X < (Position.X + size.X))
            {
                if ((pointClicked.Y > Position.Y) && (pointClicked.Y < (Position.Y + size.Y)))
                {
                    if(TextureClicked != null)
                        Texture = TextureClicked;
                    
                    return true;
                }
            }
            return false;
        }
    }
}
