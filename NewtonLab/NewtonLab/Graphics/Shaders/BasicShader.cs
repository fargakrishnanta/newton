﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpDX;
using SharpDX.DXGI;
using SharpDX.Direct3D11;
using SharpDX.D3DCompiler;

using Buffer = SharpDX.Direct3D11.Buffer;
using Device = SharpDX.Direct3D11.Device;
using MapFlags = SharpDX.Direct3D11.MapFlags;
namespace NewtonLab.Graphics.Shaders
{
    class BasicShader : IDisposable
    {
        #region Properties
        /// <summary>
        /// Vertex Shader
        /// </summary>
        public VertexShader VertexShader { get; private set; }
        /// <summary>
        /// Pixel Shader
        /// </summary>
        public PixelShader PixelShader { get; private set; } 
        /// <summary>
        /// Input Layout
        /// </summary>
        public InputLayout Layout { get; private set; }
        /// <summary>
        /// Pointer to current device
        /// </summary>
        public D3DHandler D3DHandler { get; private set; }
        #endregion

        public BasicShader(D3DHandler d3dHander, string filepath, ShaderDescription description, InputElement[] elements)
        {
            /* Store the device */
            D3DHandler = d3dHander;

            /* Compile Vertex Shader */
            ShaderBytecode vertShadByteCode = ShaderBytecode.CompileFromFile(filepath, description.VertexShaderFunction, "vs_5_0");
            VertexShader = new VertexShader(D3DHandler.Device, vertShadByteCode);
        
            /* Compile Pixel Shader */
            ShaderBytecode pixShadByteCode = ShaderBytecode.CompileFromFile(filepath, description.PixelShaderFunction, "ps_5_0");
            PixelShader = new PixelShader(D3DHandler.Device, pixShadByteCode);

            /* Set input layout using vertex shader byte code */
            ShaderSignature signature = new ShaderSignature(vertShadByteCode);
            Layout = new InputLayout(D3DHandler.Device, signature, elements);
        }

        /// <summary>
        /// Create a constant buffer
        /// </summary>
        /// <typeparam name="T">Generic Type. Usually of type ShaderBufferInfo.</typeparam>
        /// <returns>Returns a buffer for storing info into and passing to graphics card.</returns>
        public Buffer CreateBuffer<T>() where T : struct
        {
            return new Buffer(D3DHandler.Device, Utilities.SizeOf<T>(), ResourceUsage.Default, BindFlags.ConstantBuffer, CpuAccessFlags.None, ResourceOptionFlags.None, 0);
        }

        /// <summary>
        /// Apply the shaders to the device context.
        /// </summary>
        public void SetShadersToDevice()
        {
            D3DHandler.DeviceContext.InputAssembler.InputLayout = Layout;
            D3DHandler.DeviceContext.VertexShader.Set(VertexShader);
            D3DHandler.DeviceContext.PixelShader.Set(PixelShader);
        }

        /// <summary>
        /// Release Elements
        /// </summary>
        public void Dispose()
        {
            if (VertexShader != null)
            {
                VertexShader.Dispose();
            }

            if (PixelShader != null)
            {
                PixelShader.Dispose();
            }

            if (Layout != null)
            {
                Layout.Dispose();
            }
        }
    }
}
