﻿
cbuffer data :register(b0)
{
	float4x4 world;
	float4x4 worldViewProj;
	float4 lightDirection;
};

struct VS_IN
{
	float4 position : POSITION;
	float3 normal : NORMAL;
	float2 texcoord : TEXCOORD;
};

struct PS_IN
{
	float4 position : SV_POSITION;
	float3 normal : NORMAL;
	float2 texcoord : TEXCOORD;
};

//texture
Texture2D textureMap;
SamplerState textureSampler;

PS_IN VS( VS_IN input)
{
	PS_IN output = (PS_IN)0;

	output.position = mul(worldViewProj,input.position);
	output.normal=normalize(mul(world,input.normal));
	output.texcoord=input.texcoord;

	return output;
}

float4 PS( PS_IN input ) : SV_Target
{
	float4 ambient = float4(0.1f, 0.1f, 0.1f, 0.0f);
	float4 diffuse = float4(0.8f, 0.8f, 0.8f, 0.8f);
	float4 finalColor = textureMap.Sample( textureSampler, input.texcoord ) * ambient;
	float lightintensity;
	float4 lightintensityColor;

	/* Calculate the amount of light on this pixel */
	lightintensity = saturate(dot(input.normal, lightDirection));

	if (lightintensity > 0.0f)
	{
		lightintensityColor = diffuse * lightintensity;
		finalColor += saturate(lightintensityColor) * textureMap.Sample( textureSampler, input.texcoord );
	}

	return  finalColor;
}