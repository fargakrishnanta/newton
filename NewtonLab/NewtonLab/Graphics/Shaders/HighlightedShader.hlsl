﻿
cbuffer data :register(b0)
{
	float4x4 world;
	float4x4 worldViewProj;
	float4 lightDirection;
};

struct VS_IN
{
	float4 position : POSITION;
	float3 normal : NORMAL;
	float2 texcoord : TEXCOORD;
};

struct PS_IN
{
	float4 position : SV_POSITION;
	float3 normal : NORMAL;
	float2 texcoord : TEXCOORD;
};

//texture
Texture2D textureMap;
SamplerState textureSampler;

PS_IN VS( VS_IN input)
{
	PS_IN output = (PS_IN)0;

	output.position = mul(worldViewProj,input.position);
	output.normal=normalize(mul(world,input.normal));
	output.texcoord=input.texcoord;

	return output;
}

float4 PS( PS_IN input ) : SV_Target
{
	float4 highlightColor = float4(0.0f, 2.0f, 0.0f, 0.0f);

	return  saturate(saturate(0.1F + dot(input.normal,lightDirection.xyz)) * 
			textureMap.Sample( textureSampler, input.texcoord ) * highlightColor);
}