﻿using SharpDX;
using SharpDX.DirectInput;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewtonLab
{
    /// <summary>
    /// Enum for left mouse button, right mouse button,  and middle mouse button.
    /// </summary>
    public enum MouseKeys { 
        /// <summary>
        /// The leflt Mouse Button. Equates to 0.
        /// </summary>
        LButton, 
        /// <summary>
        /// The Right Mouse Button. Equates to 1.
        /// </summary>
        RButton, 
        /// <summary>
        ///  The Middle Mouse Button.  Equates to 2.
        /// </summary>
        MButton };

    class MouseHandler
    {
        public Vector2 PointClicked {get; set;}
        Mouse mouse;
        MouseState mouseStates;
        MouseState previousStates;
        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="di">The direct input device handler that will contain input states.</param>
        public MouseHandler(DirectInput di)
        {
            mouse = new Mouse(di);

            /* Initialize States */
            mouse.Acquire();
            mouseStates = mouse.GetCurrentState();
            previousStates = mouseStates;
        }

        /// <summary>
        /// Should be called each frame to save states.
        /// </summary>
        public void Update()
        {
            /* Store as previous state */
            previousStates = mouseStates;

            /* Get new mouse states */
            mouse.Acquire();
            mouseStates = mouse.GetCurrentState();

            
            PointClicked = new Vector2(mouseStates.X, mouseStates.Y);
        }

        /// <summary>
        /// Returns true if the key is down.
        /// </summary>
        /// <param name="KeyCode">The key code of the key to check. </param>
        /// <returns> Returns true if the key is down. </returns>
        public bool GetKey(MouseKeys KeyCode)
        {
            bool result = mouseStates.Buttons[(int)KeyCode];

            return result;
        }

        /// <summary>
        /// Returns true when the button has just been pressed.
        /// </summary>
        /// <param name="KeyCode">The code of the key. </param>
        /// <returns> Returns true when the button has just been pressed. </returns>
        public bool GetKeyDown(MouseKeys KeyCode)
        {
            bool result = mouseStates.Buttons[(int)KeyCode] && !previousStates.Buttons[(int)KeyCode];

            return result;
        }

        /// <summary>
        /// Returns true when a button has been released this frame.
        /// </summary>
        /// <param name="KeyCode"></param>
        /// <returns>Returns true when a button has been released this frame.</returns>
        public bool GetKeyUp(MouseKeys KeyCode)
        {
            bool result = previousStates.Buttons[(int)KeyCode] && !mouseStates.Buttons[(int)KeyCode];

            return result;
        }

        /// <summary>
        /// Returns the mouse wheel position.
        /// </summary>
        /// <returns>Returns the mouse wheel position.</returns>
        public int GetMouseScroll()
        {
            return mouseStates.Z;
        }
    }
}
