﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NewtonLab.Graphics;
using NewtonEngine;

using SharpDX;
using NewtonEngine.Matrices;
using NewtonLab.Graphics.ObjImport;
using SharpDX.Direct3D11;

using Buffer11 = SharpDX.Direct3D11.Buffer;
using NewtonEngine.Objects;
namespace NewtonLab 
{
    class GameObject : GraphicsObject
    {
        public enum BlockType
        {
            Normal,
            Raccoon,
            BrickWall,
            Floor,
        }

        public enum SphereType
        {
            Normal,
            Tennisball,
            Basketball,
        }

        /* Constants */
        const float RAD_PER_DEG = (float)Math.PI / 180.0f;
        const float RENDER_DISTANCE = 1000;

        /// <summary>
        /// Keeps track of all instances of Game Object.
        /// </summary>
        private static List<GameObject> gameObjects = new List<GameObject>();

        public static GameObjectLibrary Library = new GameObjectLibrary();
        public string Name { get; set; }

        #region Constructors
        private GameObject(GraphicsObject graphicsObject)
        {
            /* Set Graphics Handler and Physics Engine when set */
            GraphicsHandler = graphicsObject.GraphicsHandler;
            NewtonEngine = graphicsObject.NewtonEngine;
            Device = GraphicsHandler.D3DHandler.Device;

            Parts = new List<GraphicsObjectPart>(graphicsObject.Parts);

            Vertices = graphicsObject.Vertices;
            Indices = graphicsObject.Indices;

            VertexBuffer = Buffer11.Create<StaticVertex>(graphicsObject.Device, BindFlags.VertexBuffer, graphicsObject.Vertices.ToArray());
            IndexBuffer = Buffer11.Create(graphicsObject.Device, BindFlags.IndexBuffer, graphicsObject.Indices.ToArray());
            VertexSize = SharpDX.Utilities.SizeOf<StaticVertex>();

            PhysicsObject = graphicsObject.PhysicsObject;
        }

        /// <summary>
        /// Create a rendereable copy of Game Object.  Default mass, name, and movable status.
        /// </summary>
        /// <param name="gHandler">The Graphics Handler that will render this object. </param>
        /// <param name="nEngine">The Physics Engine that will update this object's position. </param>
        /// <param name="name">The name of this object. </param>
        /// <param name="graphicsObject">The graphics object that contains the render properties of this object. </param>
        /// <param name="clickable">Determines whether the object is clickable or not. </param>
        private GameObject(string name, GraphicsObject graphicsObject, bool clickable = true, GraphicsHandler gHandler = null, PhysicsEngine nEngine = null) :
            base(graphicsObject)
        {
            /* Set Graphics Handler and Physics Engine when set */
            GraphicsHandler = gHandler ?? DefaultGraphicsHandler;
            NewtonEngine = nEngine ?? DefaultPhysicsEngine;

            /* Set the name of the game object */
            Name = name;

            /* Store in list */
            AddToRenderLists(clickable);
        }
        #endregion

        #region Factories

        /// <summary>
        /// Create a Game Object that has a Sphere Physics Object.
        /// </summary>
        /// <param name="position">The initial position of the Physics Object. </param>
        /// <param name="radius">The initial radius of the Gramphics Object. </param>
        /// <param name="clickable">Determines whether this object can be clicked. </param>
        /// <param name="rendered">Determines whether this object gets placed on the render queues. </param>
        /// <param name="gHandler">The Graphics Handler that will render this object. Optional. </param>
        /// <param name="nEngine">The Physics Engine that will update this object. Optional. </param>
        /// <param name="sphereType">The type of sphere this object will be. </param>
        /// <returns> Returns a Game Object that contains a Sphere Physics Object. </returns>
        public static GameObject CreateSphere(Vector3 position, double radius, SphereType sphereType = SphereType.Normal,
            bool clickable = true, bool rendered = true, GraphicsHandler gHandler = null, PhysicsEngine nEngine = null)
        {

            GraphicsObject gObject = GraphicsObject.CreateSphere(position, radius, sphereType, gHandler, nEngine);
            GameObject newObject = new GameObject(gObject);

            /* Set the name of the game object */
            newObject.Name = "Sphere";

            /* If this object is marked to be rendered, add to render lisits */
            if (rendered)
            {
                newObject.AddToRenderLists(clickable);
            }

            return newObject;
        }

        /// <summary>
        /// Create a Game Object that has a Block Physics Object.
        /// </summary>
        /// <param name="position">The initial position of the Physics Object. </param>
        /// <param name="dimensionX">The length of the block on the X-axis.</param>
        /// <param name="dimensionY">The length of the block on the Y-axis.</param>
        /// <param name="dimensionZ">The length of the block on the Z-axis.</param>
        /// <param name="blockType">The type of block to spawn.</param>
        /// <param name="Clickable">Determines whether this object can be selected by the user.</param>
        /// <param name="rendered">Determines whether this object gets placed on the render queues. </param>
        /// <param name="gHandler">The Graphics Handler that will render this object. Optional. </param>
        /// <param name="nEngine">The Physics Engine that will update this object. Optional. </param>
        /// <returns> Returns a Game Object that contains a Block Physics Object. </returns>
        public static GameObject CreateBlock(Vector3 position, double dimensionX, double dimensionY, double dimensionZ, BlockType blockType = BlockType.Normal,
            bool Clickable = true, bool rendered = true, GraphicsHandler gHandler = null, PhysicsEngine nEngine = null)
        {
            double[] dimensions = { dimensionX, dimensionY, dimensionZ };

            GraphicsObject gObject = GraphicsObject.CreateBlock(position, dimensions, blockType, gHandler, nEngine);

            GameObject newObject = new GameObject(gObject);

            /* Set the name of the game object */
            newObject.Name = "Block";

            /* If this object is marked to be rendered, add to render lisits */
            if (rendered)
            {
                newObject.AddToRenderLists(Clickable);
            }

            return newObject;
        }

        /// <summary>
        /// Create a Game Object that has a Block Physics Object.
        /// </summary>
        /// <param name="position">The initial position of the Physics Object. </param>
        /// <param name="dimension">The initial dimensions of the Graphics Object. </param>
        /// <param name="blockType">The type of block to spawn. </param>
        /// <param name="clickable">Determines whether this object can be selected by the user. </param>
        /// <param name="rendered">Determines whether this object gets placed on the render queues. </param>
        /// <param name="gHandler">The Graphics Handler that will render this object. Optional. </param>
        /// <param name="nEngine">The Physics Engine that will update this object. Optional. </param>
        /// <returns> Returns a Game Object that contains a Block Physics Object. </returns>
        public static GameObject CreateBlock(Vector3 position, double dimension, BlockType blockType = BlockType.Normal, bool clickable = true, bool rendered = true, GraphicsHandler gHandler = null, PhysicsEngine nEngine = null)
        {
            double[] dimensions = { dimension };

            GraphicsObject gObject = GraphicsObject.CreateBlock(position, dimensions, blockType, gHandler, nEngine);

            GameObject newObject = new GameObject(gObject);

            /* Set the name of the game object */
            newObject.Name = "Block";

            /* If this object is marked to be rendered, add to render lisits */
            if (rendered)
            {
                newObject.AddToRenderLists(clickable);
            }

            return newObject;
        }

        /// <summary>
        /// Create a Game Object that has a Generic Physics Object.
        /// </summary>
        /// <param name="position">The initial position of the Physics Object. </param>
        /// <param name="filepath">The 3D model to load for this object </param>
        /// <param name="name">The name of this object. </param>
        /// <param name="rendered">Determines whether this object gets placed on the render queues. </param>
        /// <param name="gHandler">The Graphics Handler that will render this object. Optional. </param>
        /// <param name="nEngine">The Physics Engine that will update this object. Optional. </param>
        /// <returns> Returns a Game Object that contains a Generic Physics Object. </returns>
        public static GameObject CreateObject(Vector3 position, string filepath, string name, bool rendered = true, GraphicsHandler gHandler = null, PhysicsEngine nEngine = null)
        {

            GraphicsObject gObject = GraphicsObject.CreateObject(position, filepath, gHandler, nEngine);
            GameObject newObject = new GameObject(gObject);

            /* Set the name of the game object */
            newObject.Name = name;

            /* If this object is marked to be rendered, add to render lisits */
            if (rendered)
            {
                newObject.AddToRenderLists();
            }

            return newObject;
        }

        /// <summary>
        /// Creates an instance of a GameObject stored in the Library.
        /// </summary>
        /// <param name="gHandler">The Graphics Handler that will render this object. Optional. </param>
        /// <param name="nEngine">The Physics Engine that will update this object. Optional. </param>
        /// <param name="name">The name of the object to spawn. Used to search the game object library for existing copies. </param>
        /// <param name="position">The position to spawn the object. </param>
        /// <param name="movable">Determines whether this object will move or stay fixed. </param>
        /// <param name="clickable">Determines whether the user can select this object. </param>
        /// <returns> Returns a reference to the game object that has been spawned. </returns>
        public static GameObject CreateByName(string name, Vector3 position, bool movable, bool clickable = true,
            GraphicsHandler gHandler = null, PhysicsEngine nEngine = null)
        {
            if (Library.Contains(name))
            {
                GameObject masterCopy = Library.Get(name);

                GameObject copy = new GameObject(name, masterCopy, clickable, gHandler, nEngine);

                copy.Position = position;
                copy.PhysicsObject.Scaling = new Vector(masterCopy.PhysicsObject.Scaling.X, masterCopy.PhysicsObject.Scaling.Y, masterCopy.PhysicsObject.Scaling.Z);

                /* Set the fixed property only if it is not a block */
                if (copy.PhysicsObject is Block)
                {

                }
                else
                {
                    copy.Fixed = !movable;
                }

                return copy;
            }
            return null;
        }

        #endregion

        /// <summary>
        /// Add the object to the lists for rendering */
        /// </summary>
        public void AddToRenderLists(bool Clickable = true)
        {
            /* Add object to game object list, render and physics queue */
            if (Clickable)
            {
                gameObjects.Add(this);
            }

            GraphicsHandler.AddGraphicsObject(this);
            NewtonEngine.AddObject(this.PhysicsObject);
        }

        /// <summary>
        /// Add the game object to the game object library. 
        /// </summary>
        /// <param name="name">The name of the game object entry. </param>
        /// <param name="gameObject">The object to add to the library. </param>
        public static void AddToLibrary(string name, GameObject gameObject)
        {
            Library.Add(name, gameObject);
        }

        /// <summary>
        /// Rotate the game object in degrees.
        /// </summary>
        /// <param name="rotation">The rotation of all 3 axis in degrees. </param>
        public void Rotate(Vector3 rotation)
        {
            PhysicsObject.Rotate(rotation.X * RAD_PER_DEG, rotation.Y * RAD_PER_DEG, rotation.Z * RAD_PER_DEG);
        }

        /// <summary>
        /// Converts the rotation in degrees to radians
        /// </summary>
        /// <param name="rotation">The rotation to translate. </param>
        /// <returns> A new Vector3 equal to the input rotation in degrees. </returns>
        private static Vector3 ToRadians(Vector3 rotation)
        {
            return new Vector3(rotation.X * RAD_PER_DEG, rotation.Y * RAD_PER_DEG, rotation.Z * RAD_PER_DEG);
        }

        /// <summary>
        /// Finds and returns a List of game objects with matching name.
        /// </summary>
        /// <param name="name">The name to search for. </param>
        /// <returns> Returns a List of game objects with matching name. </returns>
        public static List<GameObject> FindByName(string name)
        {
            List<GameObject> result = new List<GameObject>();

            /* Search for game objects with matching names, add found to result list */
            foreach (GameObject gameObject in gameObjects)
            {
                if (String.Equals(gameObject.Name.Trim(), name.Trim(), StringComparison.OrdinalIgnoreCase))
                {
                    result.Add(gameObject);
                }
            }

            return result;
        }

        /// <summary>
        /// Kill all objects that are too far away.
        /// </summary>
        /// <param name="camera">The camera to use as reference point.</param>
        public static void CleanUpDistanceObjects(Camera camera)
        {
            List<GameObject> toDestroy = new List<GameObject>();

            foreach (GameObject gameObject in gameObjects)
            {
                if (Vector3.Distance(gameObject.Position, camera.Position) > RENDER_DISTANCE)
                {
                    toDestroy.Add(gameObject);
                }
            }

            foreach (GameObject gameObject in toDestroy)
            {
                Destroy(gameObject);
            }
            
        }

        /// <summary>
        /// Calculates and returns a list of game
        /// </summary>
        /// <param name="camera">The camera to check in front of. </param>
        /// <returns> Returns a list of Game Objects that is in front of the camera. </returns>
        public static List<GameObject> GetObjectsInFromt(Camera camera)
        {
            List<GameObject> result = new List<GameObject>();

            /* Search and test each object */
            foreach (GameObject gameObject in gameObjects)
            {
                /* Get vector from camera to the object */
                Vector3 cameraToObject = gameObject.Position - camera.Position;

                camera.LookAt.Normalize();
                cameraToObject.Normalize();

                /* If the angle of the two vectors is les than 90 degrees, add to list */
                if (Vector3.Dot(camera.LookAt, cameraToObject) > 0)
                {
                    result.Add(gameObject);
                }
            }

            return result;
        }

        /// <summary>
        /// Deletes the game object.
        /// </summary>
        /// <param name="gameObject">Game object to delete.</param>
        public static void Destroy(GameObject gameObject)
        {
            gameObject.GraphicsHandler.RemoveGraphicsObject(gameObject);
            gameObject.NewtonEngine.RemoveObject(gameObject.PhysicsObject);
            gameObjects.Remove(gameObject);
            gameObject = null;
        }
    }
}
