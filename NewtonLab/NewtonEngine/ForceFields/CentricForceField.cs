﻿using NewtonEngine.Matrices;
using System;

namespace NewtonEngine.ForceFields
{
    /// <summary>
    /// This force field causes an acceleration towards (or away from) a single point, which may be
    /// constant or depend on the distance to the source, to every object in the world. This class
    /// can be used to simulate the pulling or pushing force of magets, for example.
    /// </summary>
    public class CentricForceField : ForceField
    {
        /// <summary>
        /// The source for this force field. If the object moves, the force field moves with it.
        /// </summary>
        public PhysicsObject Source { get; private set; }

        /// <summary>
        /// The acceleration this force field imposes on objects at a distance of 1 unit.
        /// A negative value indicates an acceleration towards the source, while a positive value
        /// indicates an acceleration away from the source.
        /// </summary>
        public double Acceleration { get; set; }

        /// <summary>
        /// The type of attenuation that affects the force of the force field with increasing
        /// distance from the source.
        /// </summary>
        public Attenuation Attenuation { get; set; }
        
        /// <summary>
        /// Creates a new centric force field with the specified source object, acceleration, and
        /// type of attenuation.
        /// </summary>
        /// <param name="source">The object that acts as source for this force field.</param>
        /// <param name="acceleration">The acceleration this force field imposes on objects at a
        /// distance of 1 unit. A negative value indicates an acceleration towards the source,
        /// while a positive value indicates an acceleration away from the source.</param>
        /// <param name="attenuation">The type of attenuation that affects the force of this force
        /// field with increasing distance from the source.</param>
        public CentricForceField(PhysicsObject source, double acceleration,
            Attenuation attenuation = Attenuation.NONE)
        {
            Source       = source;
            Acceleration = acceleration;
            Attenuation  = attenuation;

            source.ForceField = this;
        }

        /// <summary>
        /// Updates the given net force by the force that the implementing force field applies to
        /// the specified object. The force is given by the acceleration times the attenuation
        /// factor times the object's mass, as a vector pointing away from or towards the source.
        /// Note that the force is zero in the centre of the source.
        /// </summary>
        /// <param name="netForce">The net force that will be updated.</param>
        /// <param name="obj">The object for which the force is calculated.</param>
        public override void UpdateForce(ref Vector netForce, PhysicsObject obj)
        {
            // Vector from source to object.
            Vector r = obj.Position - Source.Position;
            double d = r.GetLengthSquare();

            if (d == 0)
            {
                return;
            }

            double attenuationFactor = 1.0;

            switch (Attenuation)
            {
                case Attenuation.LINEAR:    attenuationFactor = 1 / Math.Sqrt(d); break;
                case Attenuation.QUADRATIC: attenuationFactor = 1 / d;            break;
            }

            netForce += Acceleration * attenuationFactor * r.Normalize() * obj.Mass;
        }
    }
}