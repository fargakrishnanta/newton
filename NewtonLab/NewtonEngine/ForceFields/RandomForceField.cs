﻿using NewtonEngine.Matrices;
using System;
using System.Collections.Generic;

namespace NewtonEngine.ForceFields
{
    /// <summary>
    /// This force field causes a randomly changing acceleration to every object in the world,
    /// simulating chaotic behaviour.
    /// </summary>
    public class RandomForceField : ForceField
    {
        private struct AccelerationData
        {
            public DateTime LastUpdate;
            public Vector   Current;
            public Vector   Previous;
        }
        
        private Random random;
        private Dictionary<PhysicsObject, AccelerationData> map;

        /// <summary>
        /// The absolute acceleration this force field imposes on objects.
        /// </summary>
        public double Acceleration { get; set; }

        /// <summary>
        /// The interval in ms after which the direction of the acceleration changes.
        /// </summary>
        public int Interval { get; set; }

        /// <summary>
        /// Creates a new random force field with the specified acceleration and time interval.
        /// </summary>
        /// <param name="acceleration">The absolute acceleration this force field imposes on
        /// objects.</param>
        /// <param name="interval">The interval in ms after which the direction of the acceleration
        /// changes.</param>
        public RandomForceField(double acceleration, int interval)
        {
            random       = new Random();
            map          = new Dictionary<PhysicsObject, AccelerationData>();
            Acceleration = acceleration;
            Interval     = interval;
        }

        /// <summary>
        /// Updates the given net force by the force that the implementing force field applies to
        /// the specified object. The force is given by the acceleration times a random unit vector
        /// times the object's mass, where the random unit vector is re-rolled for each interval.
        /// </summary>
        /// <param name="netForce">The net force that will be updated.</param>
        /// <param name="obj">The object for which the force is calculated.</param>
        public override void UpdateForce(ref Vector netForce, PhysicsObject obj)
        {
            if (!map.ContainsKey(obj) ||
                (DateTime.Now - map[obj].LastUpdate).Milliseconds >= Interval)
            {
                double x = random.NextDouble() * 2.0 - 1.0;
                double y = random.NextDouble() * 2.0 - 1.0;
                double z = random.NextDouble() * 2.0 - 1.0;
                Vector v = new Vector(x, y, z).Normalize() * Acceleration;

                AccelerationData data = new AccelerationData();

                data.LastUpdate = DateTime.Now;
                data.Previous   = v;
                data.Current    = v - (map.ContainsKey(obj) ? map[obj].Previous : Vector.NULL);
                map[obj]        = data;
            }

            netForce += map[obj].Current * obj.Mass;
        }
    }
}