﻿using NewtonEngine.Matrices;
using NewtonEngine.Objects;

namespace NewtonEngine.ForceFields
{
    /// <summary>
    /// This force field causes a resistance force to all moving objects, such as air drag.
    /// </summary>
    public class Drag : ForceField
    {
        /// <summary>
        /// Default value for the density of the medium (air).
        /// </summary>
        public const double DEFAULT_DENSITY = 1.027;

        /// <summary>
        /// Density of the medium that causes drag.
        /// </summary>
        public double Density { get; set; }

        /// <summary>
        /// Creates a new drag force field with the specified medium density.
        /// </summary>
        /// <param name="density">The density of the medium that causes drag.</param>
        public Drag(double density = DEFAULT_DENSITY)
        {
            Density = density;
        }

        /// <summary>
        /// Updates the given net force by the force that the implementing force field applies to
        /// the specified object. The force is given by 0.5 times the object's velocity squared
        /// times the object's drag coefficient times the object's cross section area.
        /// </summary>
        /// <param name="netForce">The net force that will be updated.</param>
        /// <param name="obj">The object for which the force is calculated.</param>
        public override void UpdateForce(ref Vector netForce, PhysicsObject obj)
        {
            Sphere sphere = obj as Sphere;

            if (sphere == null)
            {
                return;
            }
            else
            {
                netForce -= Density * sphere.Velocity.GetLengthSquare() * Sphere.DRAG_COEFFICIENT
                          * sphere.CrossSectionArea / 2 * sphere.Velocity.Normalize();
            }
        }
    }
}