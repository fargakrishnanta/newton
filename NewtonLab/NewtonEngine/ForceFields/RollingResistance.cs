﻿using NewtonEngine.Matrices;
using NewtonEngine.Objects;

namespace NewtonEngine.ForceFields
{
    /// <summary>
    /// This force field causes a resistance force to all objects that roll along another object.
    /// </summary>
    public class RollingResistance : ForceField
    {
        /// <summary>
        /// The default resistance coefficent for rolling.
        /// </summary>
        public const double DEFAULT_ROLLING_COEFFICIENT = 0.3;

        /// <summary>
        /// The resistance coefficient for rolling.
        /// </summary>
        public double RollingCoefficient { get; set; }

        /// <summary>
        /// Creates a new rolling resistance force field with the specified rolling coefficient.
        /// </summary>
        /// <param name="rollingCoefficient">The resistance coefficient for rolling.</param>
        public RollingResistance(double rollingCoefficient = DEFAULT_ROLLING_COEFFICIENT)
        {
            RollingCoefficient = rollingCoefficient;
        }

        /// <summary>
        /// Updates the given net force by the force that the implementing force field applies to
        /// the specified object. The force is given by the rolling coefficient times the object's
        /// normal force.
        /// </summary>
        /// <param name="netForce">The net force that will be updated.</param>
        /// <param name="obj">The object for which the force is calculated.</param>
        public override void UpdateForce(ref Vector netForce, PhysicsObject obj)
        {
            Sphere sphere = obj as Sphere;

            if (sphere == null)
            {
                return;
            }
            else
            {
                Vector n = sphere.RollingSurfaceNormal;

                if (!n.IsNull())
                {
                    Vector normalForce = Vector.DotProduct(netForce, n) * n;

                    netForce -= RollingCoefficient * normalForce.GetLength()
                                                   * sphere.Velocity.Normalize();
                }

                sphere.RollingSurfaceNormal = Vector.NULL;
            }
        }
    }
}