﻿using NewtonEngine.Algorithms;
using NewtonEngine.ForceFields;
using NewtonEngine.Matrices;
using System.Collections.Generic;

namespace NewtonEngine
{
    /// <summary>
    /// The physics engine contains a list of objects and force fields. The objects' positions are
    /// updated according to the forces that are applied by the existing force fields.
    /// </summary>
    public class PhysicsEngine
    {
        #region Fields

        private int objectCount = 0;

        private List<Collidable>    collidables;
        private List<PhysicsObject> objects;
        private List<ForceField>    forceFields;
        private OctTree             octTree;
        private int                 minUpdateInterval;
        private long                elapsedMilliseconds;

        /// <summary>
        /// The default value for minUpdateInterval if not specified, i.e. the minimum update
        /// interval for the physics engine (in ms).
        /// </summary>
        public const int DEFAULT_MIN_UPDATE_INTERVAL = 10;

        /// <summary>
        /// The default value for maxCollisionRange for collision detection if not specified.
        /// </summary>
        public const int DEFAULT_MAX_COLLISION_RANGE = 1024;

        /// <summary>
        /// The default restitution coefficient for collisions.
        /// </summary>
        public const double DEFAULT_RESTITUTION_COEFFICIENT = 1.00;

        /// <summary>
        /// Threshold for velocities. Values lower than this threshold will be treated as zero.
        /// </summary>
        internal const double MIN_VELOCITY_THRESHOLD = 1e-6;

        /// <summary>
        /// Threshold for distances. Values lower than this threshold will be treated as zero.
        /// </summary>
        internal const double MIN_DISTANCE_THRESHOLD = 1e-6;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor for the physics engine.
        /// </summary>
        /// <param name="minUpdateInterval">The minimum time interval (in ms) for updating the
        /// objects in the world. The engine will make sure that the update function is not
        /// evaluated in shorter intervals than the minimum update interval.</param>
        /// <param name="maxCollisionRange">The maximum range (in m) in which collision detection
        /// is performed.</param>
        public PhysicsEngine(
            int minUpdateInterval = DEFAULT_MIN_UPDATE_INTERVAL,
            int maxCollisionRange = DEFAULT_MAX_COLLISION_RANGE)
        {
            collidables = new List<Collidable>();
            objects     = new List<PhysicsObject>();
            forceFields = new List<ForceField>();
            octTree     = new OctTree(collidables, maxCollisionRange);

            this.elapsedMilliseconds = 0;
            this.minUpdateInterval   = minUpdateInterval;

            RestitutionCoefficient = DEFAULT_RESTITUTION_COEFFICIENT;
        }

        #endregion

        #region Properties

        /// <summary>
        /// The engine's restitution coefficient for collisions.
        /// </summary>
        public static double RestitutionCoefficient { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Adds an object to the object list.
        /// </summary>
        /// <param name="obj">The object to add.</param>
        public void AddObject(PhysicsObject obj)
        {
            obj.ID = objectCount++;
            objects.Add(obj);

            Collidable collidable = obj as Collidable;

            if (collidable != null)
            {
                collidables.Add(collidable);
            }
        }

        /// <summary>
        /// Removes an object from the object list.
        /// </summary>
        /// <param name="obj">The object to remove.</param>
        public void RemoveObject(PhysicsObject obj)
        {
            objects.Remove(obj);

            if (obj.ForceField != null)
            {
                forceFields.Remove(obj.ForceField);
            }

            Collidable collidable = obj as Collidable;
            
            if (collidable != null)
            {
                collidables.Remove(collidable);
            }
        }

        /// <summary>
        /// Adds a force field to the force field list.
        /// </summary>
        /// <param name="forceField">The force field to add.</param>
        public void AddForceField(ForceField forceField)
        {
            forceFields.Add(forceField);
        }

        /// <summary>
        /// Removes a force field from the force field list.
        /// </summary>
        /// <param name="forceField">The force field to remove.</param>
        public void RemoveForceField(ForceField forceField)
        {
            if (forceField is CentricForceField)
            {
                (forceField as CentricForceField).Source.ForceField = null;
            }
            forceFields.Remove(forceField);
        }

        /// <summary>
        /// Checks if the force field is already in the force field list.
        /// </summary>
        /// <param name="forceField">The force field to check.</param>
        /// <returns>Returns true if the forcefield is already in the force field list.</returns>
        public bool HasForceField(ForceField forceField)
        {
            return forceFields.Contains(forceField);
        }

        /// <summary>
        /// Updates the list of objects given the elapsed time by calculating the forces that apply
        /// to every non-fixed object in the world.
        /// </summary>
        /// <param name="cameraPositionX">The x-coordinate of the camera's current position.</param>
        /// <param name="cameraPositionY">The y-coordinate of the camera's current position.</param>
        /// <param name="cameraPositionZ">The z-coordinate of the camera's current position.</param>
        /// <param name="elapsedMilliseconds">The amount of time in ms that has pass since the last
        /// update call.</param>
        public void Update(double cameraPositionX, double cameraPositionY, double cameraPositionZ,
            long elapsedMilliseconds)
        {
            this.elapsedMilliseconds += elapsedMilliseconds;

            if (this.elapsedMilliseconds < minUpdateInterval)
            {
                // Not enough time passed.
                return;
            }

            // Update all non-fixed objects.
            foreach (PhysicsObject obj in objects)
            {
                if (!obj.Fixed)
                {
                    Vector netForce = new Vector();

                    foreach (ForceField forceField in forceFields)
                    {
                        if (forceField.Enabled)
                        {
                            forceField.UpdateForce(ref netForce, obj);
                        }
                    }

                    obj.ApplyForce(netForce, this.elapsedMilliseconds / 1000.0);
                }
            }

            octTree.Build(cameraPositionX, cameraPositionY, cameraPositionZ);
            octTree.CheckCollisions();

            this.elapsedMilliseconds = 0;
        }

        #endregion
    }
}