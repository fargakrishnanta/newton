﻿using NewtonEngine.Matrices;

namespace NewtonEngine
{
    /// <summary>
    /// This is the common parent class for all force field classes.
    /// </summary>
    public abstract class ForceField
    {
        /// <summary>
        /// Whether the force field is enabled.
        /// </summary>
        public bool Enabled { get; set; }

        /// <summary>
        /// Updates the given net force by the force that the implementing force field applies to
        /// the specified object.
        /// </summary>
        /// <param name="netForce">The net force that will be updated.</param>
        /// <param name="obj">The object for which the force is calculated.</param>
        public abstract void UpdateForce(ref Vector netForce, PhysicsObject obj);
    }
}