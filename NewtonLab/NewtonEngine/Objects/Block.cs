﻿using NewtonEngine.Matrices;
using System;

namespace NewtonEngine.Objects
{
    /// <summary>
    /// This class represents a rectangular block defined by (x, y, z) dimensions.
    /// </summary>
    public class Block : Collidable
    {
        #region Fields

        private Matrix vertices;

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new block at position (x, y, z) with the specified side lengths.
        /// </summary>
        /// <param name="x">The x-coordinate of the block's position.</param>
        /// <param name="y">The y-coordinate of the block's position.</param>
        /// <param name="z">The z-coordinate of the block's position.</param>
        /// <param name="dimensionX">The length of the block's x-side.</param>
        /// <param name="dimensionY">The length of the block's y-side.</param>
        /// <param name="dimensionZ">The length of the block's z-side.</param>
        public Block(double x, double y, double z, double dimensionX, double dimensionY,
            double dimensionZ) : base(x, y, z)
        {
            vertices = new Matrix(8, 4);

            vertices.SetRow(0, -0.5, -0.5, -0.5, 1);
            vertices.SetRow(1, -0.5, -0.5, +0.5, 1);
            vertices.SetRow(2, -0.5, +0.5, -0.5, 1);
            vertices.SetRow(3, -0.5, +0.5, +0.5, 1);
            vertices.SetRow(4, +0.5, -0.5, -0.5, 1);
            vertices.SetRow(5, +0.5, -0.5, +0.5, 1);
            vertices.SetRow(6, +0.5, +0.5, -0.5, 1);
            vertices.SetRow(7, +0.5, +0.5, +0.5, 1);

            Scale(dimensionX, dimensionY, dimensionZ);

            isFixed = true;
        }

        /// <summary>
        /// Creates a new cube at position (x, y, z) with the specified side length.
        /// </summary>
        /// <param name="x">The x-coordinate of the block's position.</param>
        /// <param name="y">The y-coordinate of the block's position.</param>
        /// <param name="z">The z-coordinate of the block's position.</param>
        /// <param name="dimension">The ength of the cube's sides.</param>
        public Block(double x, double y, double z, double dimension)
            : this(x, y, z, dimension, dimension, dimension)
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Whether the object is fixed or free to move. (Blocks are always fixed.)
        /// </summary>
        public override bool Fixed
        {
            get
            {
                return base.Fixed;
            }
            set
            {
                if (value != true)
                {
                    throw new InvalidOperationException("Blocks must be fixed");
                }
            }
        }

        /// <summary>
        /// The radius of the block's bounding sphere (in m).
        /// </summary>
        public override double Radius
        {
            get
            {
                return Scaling.GetLength() / 2;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Calculates and returns the matrix of vertices of the block's bounding box.
        /// </summary>
        /// <returns>The vertices of the block's bounding box.</returns>
        public override Matrix GetBoundingBox()
        {
            return vertices * GetTransformation();
        }

        /// <summary>
        /// Calculates and returns the block's volume in m^3.
        /// </summary>
        /// <returns>The block's volume in m^3.</returns>
        public override double GetVolume()
        {
            return Scaling.X * Scaling.Y * Scaling.Z;
        }

        /// <summary>
        /// Calculates and returns the normalized normal vector of the block's surface towards the
        /// given position.
        /// </summary>
        /// <param name="position">The position to get the normal vector to.</param>
        /// <returns>The normalized normal vector of the surface towards the position.</returns>
        public override Vector GetNormalTo(Vector position)
        {
            Matrix m = new Matrix(new double[,] { { position.X, position.Y, position.Z, 1 } })
                * Transformations.Translation(-Position)
                * Transformations.Rotation(-Rotation);

            Vector p = new Vector(m[0, 0], m[0, 1], m[0, 2]);
            
            double dx = Math.Max(Math.Abs(p.X) - Scaling.X / 2, 0) * Math.Sign(p.X);
            double dy = Math.Max(Math.Abs(p.Y) - Scaling.Y / 2, 0) * Math.Sign(p.Y);
            double dz = Math.Max(Math.Abs(p.Z) - Scaling.Z / 2, 0) * Math.Sign(p.Z);
            
            Matrix n = new Matrix(new double[,] { {dx, dy, dz, 0.0 } });

            n *= Transformations.Rotation(Rotation);

            return new Vector(n[0, 0], n[0, 1], n[0, 2]).Normalize();
        }

        #endregion
    }
}