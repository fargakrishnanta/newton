﻿using NewtonEngine.Exceptions;
using System;
using System.Text;

namespace NewtonEngine.Matrices
{
    /// <summary>
    /// This class represents a matrix of doubles and supports common matrix operations.
    /// </summary>
    public class Matrix
    {
        #region Fields

        // The matrix's values.
        private double[,] values;

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new matrix of the given size.
        /// </summary>
        /// <param name="numRows">The number of rows.</param>
        /// <param name="numCols">The number of columns.</param>
        public Matrix(int numRows, int numCols)
        {
            values = new double[numRows, numCols];
        }

        /// <summary>
        /// Creates a new square matrix of the given size.
        /// </summary>
        /// <param name="n">The number of rows and columns.</param>
        public Matrix(int n) : this(n, n) { }

        /// <summary>
        /// Creates a new matrix from the given two-dimensional array.
        /// </summary>
        /// <param name="values">The values for the matrix.</param>
        public Matrix(double[,] values)
        {
            this.values = values;
        }

        /// <summary>
        /// Creates a new matrix from the given two-dimensional array.
        /// </summary>
        /// <param name="values">The values for the matrix.</param>
        public Matrix(double[][] values)
        {
            this.values = new double[values.Length, values[0].Length];

            for (int i = 0; i < this.values.GetLength(0); ++i)
            {
                for (int j = 0; j < this.values.GetLength(1); ++j)
                {
                    this.values[i, j] = values[i][j];
                }
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// The number of rows.
        /// </summary>
        public int NumRows { get { return values.GetLength(0); } }

        /// <summary>
        /// The number of columns.
        /// </summary>
        public int NumCols { get { return values.GetLength(1); } }

        #endregion

        #region Operators

        /// <summary>
        /// Accesses the value at position (i, j).
        /// </summary>
        /// <param name="i">The row index of the value to access.</param>
        /// <param name="j">The column index of the value to access.</param>
        /// <returns>The value in row i and column j.</returns>
        public double this[int i, int j]
        {
            get { return values[i, j];  }
            set { values[i, j] = value; }
        }

        /// <summary>
        /// Adds the given two matrices m1 and m2 and returns their sum.
        /// </summary>
        /// <param name="m1">The left matrix to add.</param>
        /// <param name="m2">The right matrix to add.</param>
        /// <returns>The sum of m1 and m2.</returns>
        /// <exception cref="MatrixOperationException">
        /// m1 and m2 do not have same dimensions.</exception>
        public static Matrix operator +(Matrix m1, Matrix m2)
        {
            if (m1.NumRows != m2.NumRows ||
                m1.NumCols != m2.NumCols)
            {
                throw new MatrixOperationException("+/-");
            }

            Matrix sum = new Matrix(m1.NumRows, m1.NumCols);

            for (int i = 0; i < sum.NumRows; ++i)
            {
                for (int j = 0; j < sum.NumCols; ++j)
                {
                    sum[i, j] = m1[i, j] + m2[i, j];
                }
            }

            return sum;
        }

        /// <summary>
        /// Subtracts the given matrices m1 and m2 and returns their difference.
        /// </summary>
        /// <param name="m1">The left matrix to subtract.</param>
        /// <param name="m2">The right matrix to subtract.</param>
        /// <returns>The difference of m1 and m2.</returns>
        /// <exception cref="MatrixOperationException">
        /// m1 and m2 do not have same dimensions.</exception>
        public static Matrix operator -(Matrix m1, Matrix m2)
        {
            return m1 + (-m2);
        }

        /// <summary>
        /// Flips the sign of all values and returns the result.
        /// </summary>
        /// <param name="m">The matrix to apply the unary minus to.</param>
        /// <returns>The additive inverse of m.</returns>
        public static Matrix operator -(Matrix m)
        {
            return -1 * m;
        }

        /// <summary>
        /// Multiplies the given matrix by the given scalar and returns the result.
        /// </summary>
        /// <param name="scalar">The scalar to multiply the matrix by.</param>
        /// <param name="m">The matrix to be multiplied.</param>
        /// <returns>The matrix multiplied by the scalar.</returns>
        public static Matrix operator *(double scalar, Matrix m)
        {
            Matrix product = new Matrix(m.NumRows, m.NumCols);

            for (int i = 0; i < m.NumRows; ++i)
            {
                for (int j = 0; j < m.NumCols; ++j)
                {
                    product[i, j] = scalar * m[i, j];
                }
            }

            return product;
        }

        /// <summary>
        /// Multiplies the given matrix by the given scalar and returns the result.
        /// </summary>
        /// <param name="m">The matrix to be multiplied.</param>
        /// <param name="scalar">The scalar to multiply the matrix by.</param>
        /// <returns>The matrix multiplied by the scalar.</returns>
        public static Matrix operator *(Matrix m, double scalar)
        {
            return scalar * m;
        }

        /// <summary>
        /// Divides the given matrix by the given scalar and returns the result.
        /// </summary>
        /// <param name="m">The matrix to be divided.</param>
        /// <param name="scalar">The scalar to dvide the matrix by.</param>
        /// <returns>The matrix divided by the scalar.</returns>
        public static Matrix operator /(Matrix m, double scalar)
        {
            return (1.0 / scalar) * m;
        }

        /// <summary>
        /// Multiplies the given two matrices m1 and m2 and returns their product.
        /// </summary>
        /// <param name="m1">The left matrix to multiply.</param>
        /// <param name="m2">The right matrix to multiply.</param>
        /// <returns>The product of m1 and m2.</returns>
        /// <exception cref="MatrixOperationException">
        /// m1.NumRows and m2.NumCols are not equal.</exception>
        public static Matrix operator *(Matrix m1, Matrix m2)
        {
            if (m1.NumCols != m2.NumRows)
            {
                throw new MatrixOperationException("*");
            }

            Matrix product = new Matrix(m1.NumRows, m2.NumCols);

            for (int i = 0; i < product.NumRows; ++i)
            {
                for (int j = 0; j < m1.NumCols; ++j)
                {
                    for (int k = 0; k < product.NumCols; ++k)
                    {
                        product[i, k] += m1[i, j] * m2[j, k];
                    }
                }
            }

            return product;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns the matrix's row with the given index.
        /// </summary>
        /// <param name="i">The index of the row to return.</param>
        /// <returns>The i-th row of the matrix.</returns>
        public double[] GetRow(int i)
        {
            double[] row = new double[NumCols];

            for (int j = 0; j < NumCols; ++j)
            {
                row[j] = this[i, j];
            }

            return row;
        }

        /// <summary>
        /// Returns the matrix'x column with the given index.
        /// </summary>
        /// <param name="j">The index of the column to return.</param>
        /// <returns>The j-th column of the matrix.</returns>
        public double[] GetCol(int j)
        {
            double[] col = new double[NumRows];

            for (int i = 0; i < NumRows; ++i)
            {
                col[i] = this[i, j];
            }

            return col;
        }

        /// <summary>
        /// Sets the matrix's row with the given index to the given values.
        /// </summary>
        /// <param name="i">The index of the row to set.</param>
        /// <param name="values">The new values of the i-th row.</param>
        /// <exception cref="ArgumentException">values.Length is not equal to NumCols.</exception>
        public void SetRow(int i, params double[] values)
        {
            if (values.Length != NumCols)
            {
                throw new ArgumentException();
            }
            
            for (int j = 0; j < NumCols; ++j)
            {
                this[i, j] = values[j];
            }
        }

        /// <summary>
        /// Sets the matrix's column with the given index to the given values.
        /// </summary>
        /// <param name="j">The index of the column to set.</param>
        /// <param name="values">The new values of the j-th column.</param>
        /// <exception cref="ArgumentException">values.Length is not equal to NumRows.</exception>
        public void SetCol(int j, params double[] values)
        {
            if (values.Length != NumRows)
            {
                throw new ArgumentException();
            }

            for (int i = 0; i < NumRows; ++i)
            {
                this[i, j] = values[i];
            }
        }

        /// <summary>
        /// Converts the matrix into a one-dimenional array of floats. The values will be arranged
        /// row by row in the array.
        /// </summary>
        /// <returns>An array containing the values of the matrix.</returns>
        public float[] ToFloatArray()
        {
            float[] array = new float[NumRows * NumCols];

            for (int i = 0; i < array.Length; ++i)
            {
                array[i] = (float) values[i / NumCols, i % NumCols];
            }

            return array;
        }

        /// <summary>
        /// Returns the matrix's components as a string.
        /// </summary>
        /// <returns>A string representation of the matrix.</returns>
        public override string ToString()
        {
            StringBuilder s = new StringBuilder();

            for (int i = 0; i < NumRows; ++i)
            {
                s.Append("[");

                for (int j = 0; j < NumCols; ++j)
                {
                    s.AppendFormat(" {0,5:f1}", this[i, j]);
                }

                s.Append(" ]\n");
            }

            return s.ToString();
        }

        #endregion
    }
}