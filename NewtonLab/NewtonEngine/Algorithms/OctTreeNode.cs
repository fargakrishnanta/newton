﻿using NewtonEngine.Matrices;
using System;
using System.Collections.Generic;

namespace NewtonEngine.Algorithms
{
    /// <summary>
    /// This is a node of an OctTree.
    /// </summary>
    internal class OctTreeNode
    {
        #region Fields

        private const double MIN_REGION_SIZE = 1;

        private OctTreeNode   parent;
        private OctTreeNode[] children;
        private int           halfSideLength;
        private double        centreX;
        private double        centreY;
        private double        centreZ;

        private List<Collidable> nodeObjects;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor for an OctTree node.
        /// </summary>
        /// <param name="objects">The list of objects to be checked by this node.</param>
        /// <param name="parent">The node's parent node.</param>
        /// <param name="halfSideLength">Half the side length of the node octant.</param>
        /// <param name="centreX">The x-coordinate of the node octant's centre.</param>
        /// <param name="centreY">The y-coordinate of the node octant's centre.</param>
        /// <param name="centreZ">The z-coordinate of the node octant's centre.</param>
        public OctTreeNode(List<Collidable> objects, OctTreeNode parent, int halfSideLength,
            double centreX, double centreY, double centreZ)
        {         
            this.parent         = parent;
            this.halfSideLength = halfSideLength;
            this.centreX        = centreX;
            this.centreY        = centreY;
            this.centreZ        = centreZ;

            nodeObjects = new List<Collidable>();

            if (objects.Count == 1)
            {
                nodeObjects = objects;
            }
            else if (objects.Count > 1)
            {
                Populate(objects);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Populates the the current node of the OctTree as well as its child nodes.
        /// </summary>
        /// <param name="remainingObjects">The list of remaining objects to be checked.</param>
        private void Populate(List<Collidable> remainingObjects)
        {
            if (2 * halfSideLength < MIN_REGION_SIZE)
            {
                // Lower limit for octant size reached.
                nodeObjects.AddRange(remainingObjects);
                remainingObjects.Clear();
                return;
            }
            
            // Check intersection from splitting the mode contant into 8 sub-octants.

            for (int i = remainingObjects.Count; i > 0; --i)
            {
                Collidable obj = remainingObjects[i - 1];
                
                if (CheckIntersection(obj))
                {
                    // The intersection of the octant hit an object.
                    nodeObjects.Add(obj); 
                    remainingObjects.RemoveAt(i - 1);
                }
            }

            List<Collidable>[] lists = new List<Collidable>[8];

            for (int i = 0; i < 8; ++i)
            {
                lists[i] = new List<Collidable>();
            }

            foreach (Collidable obj in remainingObjects)
            {
                Vector position = obj.Position;

                int index = 0;
                int xMask = (position.X < centreX ? 0 : 1) << 2;
                int yMask = (position.Y < centreY ? 0 : 1) << 1;
                int zMask =  position.Z < centreZ ? 0 : 1;
                index = xMask | yMask | zMask;
                lists[index].Add(obj);
            }

            int d = halfSideLength / 2;

            children    = new OctTreeNode[8];
            children[0] = new OctTreeNode(lists[0], this, d, centreX - d, centreY - d, centreZ - d);
            children[1] = new OctTreeNode(lists[1], this, d, centreX - d, centreY - d, centreZ + d);
            children[2] = new OctTreeNode(lists[2], this, d, centreX - d, centreY + d, centreZ - d);
            children[3] = new OctTreeNode(lists[3], this, d, centreX - d, centreY + d, centreZ + d);
            children[4] = new OctTreeNode(lists[4], this, d, centreX + d, centreY - d, centreZ - d);
            children[5] = new OctTreeNode(lists[5], this, d, centreX + d, centreY - d, centreZ + d);
            children[6] = new OctTreeNode(lists[6], this, d, centreX + d, centreY + d, centreZ - d);
            children[7] = new OctTreeNode(lists[7], this, d, centreX + d, centreY + d, centreZ + d);
        }

        /// <summary>
        /// Checks whether the given object intersects with any of the dividing planes of the
        /// current node's octant.
        /// </summary> 
        /// <param name="obj">The object to be checked for instersection.</param>
        /// <returns>Whether the object intersects a dividing plane.</returns>
        private bool CheckIntersection(Collidable obj)
        {
            Matrix   vertices = obj.GetBoundingBox();
            double[] vertex   = vertices.GetRow(0);

            double maxX = vertex[0];
            double minX = vertex[0];
            double maxY = vertex[1];
            double minY = vertex[1];
            double maxZ = vertex[2];
            double minZ = vertex[2];

            // Get the min and max values for x, y, z.
            for (int i = 1; i < vertices.NumRows; ++i)
            {
                vertex = vertices.GetRow(i);

                minX = Math.Min(vertex[0], minX);
                maxX = Math.Max(vertex[0], maxX);
                minY = Math.Min(vertex[1], minY);
                maxY = Math.Max(vertex[1], maxY);
                minZ = Math.Min(vertex[2], minZ);
                maxZ = Math.Max(vertex[2], maxZ);
            }

            if (minX <= centreX && maxX >= centreX ||
                minY <= centreY && maxY >= centreY ||
                minZ <= centreZ && maxZ >= centreZ)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Checks the collisions for every collidable object.
        /// </summary>
        /// <param name="parentObjects">The list of objects passed down by the parent node.</param>
        public void CheckCollision(List<Collidable> parentObjects)
        {
            List<Collidable> cumulativeObjects = new List<Collidable>(parentObjects);
            
            for (int i = 0; i < nodeObjects.Count - 1; ++i)
            {
                for (int j = i + 1; j < nodeObjects.Count; ++j)
                {
                    Collidable.Check(nodeObjects[i], nodeObjects[j]);
                }
            }

            foreach (Collidable obj1 in cumulativeObjects)
            {
                foreach (Collidable obj2 in nodeObjects)
                {
                    Collidable.Check(obj1, obj2);
                }
            }

            cumulativeObjects.AddRange(nodeObjects);
            
            if (children != null)
            {
                foreach (OctTreeNode child in children)
                {
                    child.CheckCollision(cumulativeObjects);
                }
            }
        }

        #endregion
    }
}