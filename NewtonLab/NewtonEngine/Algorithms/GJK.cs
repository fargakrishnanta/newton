﻿using NewtonEngine.Matrices;
using System;
using System.Collections.Generic;

namespace NewtonEngine.Algorithms
{
    /// <summary>
    /// Implements the Gilbert-Johnson-Keerthi algorithm for collision detection in 3D.
    /// Based on the video lecture at http://mollyrocket.com/849.
    /// </summary>
    internal static class GJK
    {
        // Maximum number of iterations to prevent infinite loops.
        private const int MAX_ITERATIONS = 20;

        /// <summary>
        /// Calculates whether two convex bodies given by the two sets of vertices intersect.
        /// </summary>
        /// <param name="vertices1">The vertices of the 1st convex body.</param>
        /// <param name="vertices2">The vertices of the 2nd convex body.</param>
        /// <returns>Whether the bodies defined by vertices1 and vertices2 intersect.</returns>
        public static bool Intersect(IList<Vector> vertices1, IList<Vector> vertices2)
        {
            Vector s  = Support(vertices1, vertices2, vertices1[0] - vertices2[0]);

            List<Vector> simplex = new List<Vector>();
            simplex.Add(s);

            Vector d = -s;

            for (int i = 0; i < MAX_ITERATIONS; ++i)
            {
                Vector a = Support(vertices1, vertices2, d);

                if (Vector.DotProduct(a, d) < 0)
                {
                    return false;
                }

                simplex.Add(a);

                if (DoSimplex(simplex, ref d))
                {
                    return true;
                }
            }

            return false;
        }
        
        /// <summary>
        /// Updates the current simplex and the direction in which to look for the origin and
        /// returns whether the simplex contains the origin.
        /// </summary>
        /// <param name="simplex">The points of the current simplex.</param>
        /// <param name="d">The direction in which to look.</param>
        /// <returns>Whether the simplex contains the origin.</returns>
        private static bool DoSimplex(List<Vector> simplex, ref Vector d)
        {
            // The simplex is a line.
            if (simplex.Count == 2)
            {
                Vector A  = simplex[1];
                Vector B  = simplex[0];
                Vector AO = -A;
                Vector AB = B - A;

                if (Vector.DotProduct(AB, AO) > 0)
                {
                    d = Vector.CrossProduct(Vector.CrossProduct(AB, AO), AB);
                }
                else
                {
                    d = AO;
                }
            }
            // The simplex is a triangle.
            else if (simplex.Count == 3)
            {
                Vector A   = simplex[2];
                Vector B   = simplex[1];
                Vector C   = simplex[0];
                Vector AO  = -A;
                Vector AB  = B - A;
                Vector AC  = C - A;
                Vector ABC = Vector.CrossProduct(AB, AC);

                if (Vector.DotProduct(Vector.CrossProduct(ABC, AC), AO) > 0)
                {
                    if (Vector.DotProduct(AC, AO) > 0)
                    {
                        simplex.Clear();
                        simplex.Add(C);
                        simplex.Add(A);
                        d = Vector.CrossProduct(Vector.CrossProduct(AC, AO), AC);
                    }
                    else if (Vector.DotProduct(AB, AO) > 0)
                    {
                        simplex.Clear();
                        simplex.Add(B);
                        simplex.Add(A);
                        d = Vector.CrossProduct(Vector.CrossProduct(AB, AO), AB);
                    }
                    else
                    {
                        simplex.Clear();
                        simplex.Add(A);
                        d = AO;
                    }
                }
                else
                {
                    if (Vector.DotProduct(Vector.CrossProduct(AB, ABC), AO) > 0)
                    {
                        if (Vector.DotProduct(AB, AO) > 0)
                        {
                            simplex.Clear();
                            simplex.Add(B);
                            simplex.Add(A);
                            d = Vector.CrossProduct(Vector.CrossProduct(AB, AO), AB);
                        }
                        else
                        {
                            simplex.Clear();
                            simplex.Add(A);
                            d = AO;
                        }
                    }
                    else
                    {
                        if (Vector.DotProduct(ABC, AO) > 0)
                        {
                            d = ABC;
                        }
                        else
                        {
                            simplex.Clear();
                            simplex.Add(B);
                            simplex.Add(C);
                            simplex.Add(A);
                            d = -ABC;
                        }
                    }
                }
            }
            // The simplex is a tetrahedron.
            else
            {
                Vector A   = simplex[3];
                Vector B   = simplex[2];
                Vector C   = simplex[1];
                Vector D   = simplex[0];
                Vector AO  = -A;
                Vector AB  = B - A;
                Vector AC  = C - A;
                Vector AD  = D - A;
                Vector ABC = Vector.CrossProduct(AB, AC);
                Vector ACD = Vector.CrossProduct(AC, AD);
                Vector ADB = Vector.CrossProduct(AD, AB);

                int BsideOnACD = Math.Sign(Vector.DotProduct(ACD, AB));
                int CsideOnADB = Math.Sign(Vector.DotProduct(ADB, AC));
                int DsideOnABC = Math.Sign(Vector.DotProduct(ABC, AD));

                bool ABsameAsOrigin = Math.Sign(Vector.DotProduct(ACD, AO)) == BsideOnACD;
                bool ACsameAsOrigin = Math.Sign(Vector.DotProduct(ADB, AO)) == CsideOnADB;
                bool ADsameAsOrigin = Math.Sign(Vector.DotProduct(ABC, AO)) == DsideOnABC;

                if (ABsameAsOrigin && ACsameAsOrigin && ADsameAsOrigin)
                {
                    return true;
                }
                else if (!ABsameAsOrigin)
                {
                    simplex.Remove(B);
                    d = ACD * -BsideOnACD;
                }
                else if (!ACsameAsOrigin)
                {
                    simplex.Remove(C);
                    d = ADB * -CsideOnADB;
                }
                else
                {
                    simplex.Remove(D);
                    d = ABC * -DsideOnABC;
                }

                return DoSimplex(simplex, ref d);
            }

            return false;
        }

        /// <summary>
        /// Finds the support of d, the farthest point of the "Minkowski difference" of two given
        /// convex bodies along the given direction d.
        /// </summary>
        /// <param name="vertices1">The vertices of the 1st convex body.</param>
        /// <param name="vertices2">The vertices of the 2nd convex body.</param>
        /// <param name="d">The direction in which to look.</param>
        /// <returns>The support of d.</returns>
        private static Vector Support(IList<Vector> vertices1, IList<Vector> vertices2, Vector d)
        {
            return MaxDotProduct(vertices1, d) - MaxDotProduct(vertices2, -d);
        }
        
        /// <summary>
        /// Finds the farthest point of a convex body along a given direction d.
        /// </summary>
        /// <param name="vertices">The vertices of the convex body.</param>
        /// <param name="d">The direction in which to look.</param>
        /// <returns>The farthest point along the direction.</returns>
        private static Vector MaxDotProduct(IList<Vector> vertices, Vector d)
        {
            Vector max = vertices[0];

            foreach (Vector v in vertices)
            {
                if (Vector.DotProduct(v, d) > Vector.DotProduct(max, d))
                {
                    max = v;
                }
            }

            return max;
        }
    }
}