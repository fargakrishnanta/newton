﻿using NewtonEngine.Matrices;
using NewtonEngine.Objects;
using System;

namespace NewtonEngine.Algorithms
{
    /// <summary>
    /// This class provides helper methods for collision detection and collision response.
    /// </summary>
    internal static class Collisions
    {
        #region Collision Detection

        /// <summary>
        /// Determines and returns whether the two given objects intersect each other. If they do,
        /// the time that has passed since they first touched is passed back through t.
        /// </summary>
        /// <param name="obj1">The 1st object to check.</param>
        /// <param name="obj2">The 2nd object to check.</param>
        /// <param name="t">The time that has passed since the objects first touched. If they do
        /// not intersect or were not moving, t will be zero.</param>
        /// <returns>Whether obj1 and obj2 intersect.</returns>
        public static bool Intersect(Collidable obj1, Collidable obj2, out double t)
        {
            if (obj1 is Sphere)
            {
                if (obj2 is Sphere)
                {
                    return Intersect((Sphere)obj1, (Sphere)obj2, out t);
                }
                else if (obj2 is Block)
                {
                    return Intersect((Sphere)obj1, (Block)obj2, out t);
                }
            }
            else if (obj1 is Block)
            {
                if (obj2 is Sphere)
                {
                    return Intersect((Sphere)obj2, (Block)obj1, out t);
                }
                else if (obj2 is Block)
                {
                    return Intersect((Block)obj1, (Block)obj2, out t);
                }
            }
            
            throw new Exception("Collisions.Intersect caused an unexpected error.");
        }

        /// <summary>
        /// Determines and returns whether the two given spheres intersect each other. If they do,
        /// the time that has passed since they first touched is passed back through t.
        /// </summary>
        /// <param name="s1">The 1st sphere to check.</param>
        /// <param name="s2">The 2nd sphere to check.</param>
        /// <param name="t">The time that has passed since the objects first touched. If they do
        /// not intersect or were not moving, t will be zero.</param>
        /// <returns>Whether s1 and s2 intersect.</returns>
        private static bool Intersect(Sphere s1, Sphere s2, out double t)
        {
            t = 0.0;
            
            // Consider s2 relative to s1.
            Vector p = s2.Position - s1.Position;
            double r = s1.Radius + s2.Radius;
            double d = p.GetLengthSquare() - r * r;

            // Check for collision.
            if (d > 0.0)
            {
                return false;
            }

            Vector v = s2.Velocity - s1.Velocity;

            if (v.GetLengthSquare() == 0.0)
            {
                // Unable to resolve collision properly, separate objects instead.
                double k = (r + PhysicsEngine.MIN_DISTANCE_THRESHOLD - p.GetLength()) / 2;
                Vector n = p.Normalize();

                if (n.IsNull())
                {
                    n = new Vector(1, 0, 0);
                }

                if (s1.Fixed)
                {
                    s2.Position += 2 * k * n;
                }
                else if (s2.Fixed)
                {
                    s1.Position -= 2 * k * n;
                }
                else
                {
                    s1.Position -= k * n;
                    s2.Position += k * n;
                }

                return false;
            }

            /* Ignoring acceleration, the condition for t when the two spheres touch is given by:
             * 
             * |p - v * t| = r
             * 
             * This results in the following quadratic equation:
             * 
             * |v|^2 * t^2 - 2 * (v·p) * t + |p|^2 - r^2 = 0
             * 
             * The larger of the two solutions for t will yield an approximate solution for the
             * time that has passed since the spheres first touched.
             */

            r += PhysicsEngine.MIN_DISTANCE_THRESHOLD;

            double a = v.GetLengthSquare();
            double b = -2 * Vector.DotProduct(v, p);
            double c = p.GetLengthSquare() - r * r;

            if (!SolveQuadraticEquation(a, b, c, out t))
            {
                throw new Exception("Unexpected error: No solution found!");
            }

            return true;
        }

        /// <summary>
        /// Determines and returns whether the given sphere and block intersect each other. If they
        /// do, the time that has passed since they first touched is passed back through t.
        /// </summary>
        /// <param name="sphere">The sphere to check.</param>
        /// <param name="block">The block to check.</param>
        /// <param name="t">The time that has passed since the objects first touched. If they do
        /// not intersect or were not moving, t will be zero.</param>
        /// <returns>Whether sphere and block intersect.</returns>
        private static bool Intersect(Sphere sphere, Block block, out double t)
        {
            t = 0.0;

            // Consider the sphere relative to the block.
            Vector p = new Vector(
                sphere.Position.ToMatrix(1.0)
                * Transformations.Translation(-block.Position)
                * Transformations.Rotation(-block.Rotation));

            double sx = block.Scaling.X / 2;
            double sy = block.Scaling.Y / 2;
            double sz = block.Scaling.Z / 2;

            // Distance between sphere and block: sqrt(dx^2 + dy^2 + dz^2)
            double dx = Math.Max(Math.Abs(p.X) - sx, 0);
            double dy = Math.Max(Math.Abs(p.Y) - sy, 0);
            double dz = Math.Max(Math.Abs(p.Z) - sz, 0);
            double d  = dx * dx + dy * dy + dz * dz;
            
            // Check for collision.
            if (d > sphere.Radius * sphere.Radius)
            {
                return false;
            }

            Vector v = sphere.Velocity - block.Velocity;

            if (v.GetLengthSquare() == 0.0)
            {
                // Unable to resolve collision properly, separate objects instead.
                double k = (sphere.Radius + PhysicsEngine.MIN_DISTANCE_THRESHOLD - Math.Sqrt(d)) / 2;
                Vector n = p.Normalize();

                if (n.IsNull())
                {
                    n = new Vector(1, 0, 0);
                }

                if (block.Fixed)
                {
                    sphere.Position += k * n;
                }
                else if (sphere.Fixed)
                {
                    block.Position -= 2 * k * n;
                }
                else
                {
                    block.Position -= k * n;
                    sphere.Position += k * n;
                }

                return false;
            }

            /* Ignoring acceleration, the condition for t when the sphere and the block touch is
             * given by:
             * 
             * dist(p - v * t, block) = sphere.Radius
             * 
             * This results in 27 quadratic equations given by:
             * 
             * (1) dx = max(|p.X - v.X * t| - sx, 0)
             * (2) dy = max(|p.Y - v.Y * t| - sy, 0)
             * (3) dz = max(|p.Z - v.Z * t| - sz, 0)
             * (4) dx^2 + dy^2 + dz^2 - sphere.Radius^2 = 0
             * 
             * The larger of the two solutions for t will yield an approximate solution for the
             * time that has passed since the sphere and the block first touched.
             */

            double r = sphere.Radius + PhysicsEngine.MIN_DISTANCE_THRESHOLD;

            for (int i = 0; i < 3; ++i)
            {
                // i = 0: case |p.X - v.X * t| - sx > 0  and  p.X - v.X * t > 0
                // i = 1: case |p.X - v.X * t| - sx > 0  and  p.X - v.X * t ≤ 0
                // i = 2: case |p.X - v.X * t| - sx ≤ 0
                    
                for (int j = 0; j < 3; ++j)
                {
                    // j = 0: case |p.Y - v.Y * t| - sy > 0  and  p.Y - v.Y * t > 0
                    // j = 1: case |p.Y - v.Y * t| - sy > 0  and  p.Y - v.Y * t ≤ 0
                    // j = 2: case |p.Y - v.Y * t| - sy ≤ 0

                    for (int k = 0; k < 3; ++k)
                    {
                        // k = 0: case |p.Z - v.Z * t| - sz > 0  and  p.Z - v.Z * t > 0
                        // k = 1: case |p.Z - v.Z * t| - sz > 0  and  p.Z - v.Z * t ≤ 0
                        // k = 2: case |p.Z - v.Z * t| - sz ≤ 0

                        double px = i < 2 ? (i == 0 ? p.X - sx : p.X + sx) : 0.0;
                        double py = j < 2 ? (j == 0 ? p.Y - sy : p.Y + sy) : 0.0;
                        double pz = k < 2 ? (k == 0 ? p.Z - sz : p.Z + sz) : 0.0;
                            
                        double a = (i < 2 ? v.X * v.X : 0.0)
                                 + (j < 2 ? v.Y * v.Y : 0.0)
                                 + (k < 2 ? v.Z * v.Z : 0.0);

                        double b = - 2 * (v.X * px + v.Y * py + v.Z * pz);
                        double c = px * px + py * py + pz * pz - r * r;

                        if (!SolveQuadraticEquation(a, b, c, out t))
                        {
                            // No solution found for this equation, so go to next equation.
                            continue;
                        }

                        Vector q = p - v * t;

                        // Verify the case conditions for this solution.
                        bool ex = Math.Abs(q.X) > sx;
                        bool ey = Math.Abs(q.Y) > sy;
                        bool ez = Math.Abs(q.Z) > sz;
                        bool fx = q.X > 0.0;
                        bool fy = q.Y > 0.0;
                        bool fz = q.Z > 0.0;

                        if ((i < 2 ? ex && (i == 0 ? fx : !fx) : !ex) &&
                            (j < 2 ? ey && (j == 0 ? fy : !fy) : !ey) &&
                            (k < 2 ? ez && (k == 0 ? fz : !fz) : !ez))
                        {
                            return true;
                        }
                    }
                }
            }

            throw new Exception("Unexpected error: No solution found!");
        }

        /// <summary>
        /// Determines and returns whether the two given blocks intersect each other. If they do,
        /// the time that has passed since they first touched is passed back through t.
        /// </summary>
        /// <param name="b1">The 1st block to check.</param>
        /// <param name="b2">The 2nd block to check.</param>
        /// <param name="t">The time that has passed since the objects first touched. If they do
        /// not intersect or were not moving, t will be zero.</param>
        /// <returns>Whether b1 and b2 intersect.</returns>
        private static bool Intersect(Block b1, Block b2, out double t)
        {
            //return GJK.Intersect(
            //    Vector.ListFromMatrix(b1.BoundingBox),
            //    Vector.ListFromMatrix(b2.BoundingBox));

            throw new NotImplementedException("Collision between blocks is not available.");
        }

        /// <summary>
        /// Returns whether the quadratic equation ax^2 + bx + c = 0 has a solution and passes back
        /// the larger of possibly two solutions for x.
        /// </summary>
        /// <param name="a">The 1st coefficient of the quadratic equation.</param>
        /// <param name="b">The 2nd coefficient of the quadratic equation.</param>
        /// <param name="c">The 3rd coefficient of the quadratic equation.</param>
        /// <param name="x">The larger of the equation's two possible solutions.</param>
        /// <returns>Whether a solution for the equation exists.</returns>
        private static bool SolveQuadraticEquation(double a, double b, double c, out double x)
        {
            x = 0.0;
            
            if (a != 0.0)
            {
                double discriminant = b * b - 4 * a * c;

                if (discriminant >= 0.0)
                {
                    x = (-b + Math.Sqrt(discriminant)) / (2 * a);
                    return true;
                }
            }
            else if (b != 0.0)
            {
                x = -c / b;
                return true;
            }
            else
            {
                return c == 0.0;
            }

            return false;
        }

        #endregion

        #region Collision Response

        /// <summary>
        /// Resolves the collision between the two given objects by calculating their new
        /// velocities resulting from the collision.
        /// </summary>
        /// <param name="obj1">The 1st colliding object to update.</param>
        /// <param name="obj2">The 2nd colliding object to update.</param>
        public static void Resolve(Collidable obj1, Collidable obj2)
        {
            Vector d = Vector.NULL;
            
            if (obj1 is Sphere)
            {
                if (obj2 is Sphere)
                {
                    d = obj1.GetNormalTo(obj2.Position);
                }
                else if (obj2 is Block)
                {
                    d = obj2.GetNormalTo(obj1.Position);
                }
            }
            else if (obj1 is Block)
            {
                if (obj2 is Sphere)
                {
                    d = obj1.GetNormalTo(obj2.Position);
                }
                else if (obj2 is Block)
                {
                    throw new NotImplementedException("Block/block collision not available");
                }
            }

            Vector v1 = Vector.DotProduct(obj1.Velocity, d) * d;
            Vector v2 = Vector.DotProduct(obj2.Velocity, d) * d;

            if (!obj1.Fixed)
            {
                if (obj2.Fixed)
                {
                    obj1.Velocity += GetVelocityDelta(v1);
                }
                else
                {
                    obj1.Velocity += GetVelocityDelta(v1, v2, obj1.Mass, obj2.Mass);
                }
            }

            if (!obj2.Fixed)
            {
                if (obj1.Fixed)
                {
                    obj2.Velocity += GetVelocityDelta(v2);
                }
                else
                {
                    obj2.Velocity += GetVelocityDelta(v2, v1, obj2.Mass, obj1.Mass);
                }
            }
        }

        /// <summary>
        /// Calculates the velocity delta for an object with velocity v in collision direction that
        /// collides with a fixed object (i.e. an object with infinite mass).
        /// </summary>
        /// <param name="v">The object's velocity in collision direction.</param>
        /// <returns>The velocity delta after collision.</returns>
        private static Vector GetVelocityDelta(Vector v)
        {
            return -(1 + PhysicsEngine.RestitutionCoefficient) * v;
        }

        /// <summary>
        /// Calculates the velocity delta for an object with mass m1 and velocity v1 in collision
        /// direction that collides with another object with mass m2 and velocity v2 in collision
        /// direction.
        /// </summary>
        /// <param name="v1">The 1st object's velocity in collision direction.</param>
        /// <param name="v2">The 2nd object's velocity in collision direction.</param>
        /// <param name="m1">The 1st object's mass.</param>
        /// <param name="m2">The 2nd object's mass.</param>
        /// <returns>The velocity delta after collision.</returns>
        private static Vector GetVelocityDelta(Vector v1, Vector v2, double m1, double m2)
        {
            return ((m1 - PhysicsEngine.RestitutionCoefficient * m2) / (m1 + m2) - 1) * v1
                   + (1 + PhysicsEngine.RestitutionCoefficient) * m2  / (m1 + m2) * v2;
        }

        #endregion
    }
}