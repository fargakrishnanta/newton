﻿using System;

namespace NewtonEngine.Exceptions
{
    /// <summary>
    /// This exception is thrown when an operation is performed on matrices with incompatible
    /// dimensions, such as when performing a matrix addition or multiplication.
    /// </summary>
    public class MatrixOperationException : Exception
    {
        private const string ERROR_MESSAGE =
            "Operation {0} performed on matrices with incompatible dimensions.";

        /// <summary>
        /// Creates a new exception for the specified operation.
        /// </summary>
        /// <param name="operation">A description of the invalid operation.</param>
        public MatrixOperationException(string operation)
            : base(String.Format(ERROR_MESSAGE, operation)) { }
    }
}