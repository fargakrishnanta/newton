﻿using NewtonEngine.Exceptions;
using NewtonEngine.ForceFields;
using NewtonEngine.Matrices;
using System;

namespace NewtonEngine
{
    /// <summary>
    /// This class describes any object in the world that is handled by the physics engine. The
    /// main properties of each object are its position, velocity, and acceleration. Forces can
    /// be applied to an object to change these properties.
    /// </summary>
    public class PhysicsObject
    {
        #region Fields

        /// <summary>
        /// Whether the object is fixed or free to move.
        /// </summary>
        protected bool isFixed;

        private double mass;
        private Vector velocity;

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new physics object at position (0, 0, 0).
        /// </summary>
        public PhysicsObject()
        {
            isFixed      = false;
            mass         = 1.0;
            Scaling      = new Vector(1.0, 1.0, 1.0);
            Rotation     = Vector.NULL;
            Acceleration = Vector.NULL;
            velocity     = Vector.NULL;
            Position     = Vector.NULL;
        }

        /// <summary>
        /// Creates a new physics object at position (x, y, z).
        /// </summary>
        /// <param name="x">The x-coordinate of the object's position.</param>
        /// <param name="y">The y-coordinate of the object's position.</param>
        /// <param name="z">The z-coordinate of the object's position.</param>
        public PhysicsObject(double x, double y, double z) : this()
        {
            Position.X = x;
            Position.Y = y;
            Position.Z = z;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Whether the object is fixed or free to move.
        /// </summary>
        public virtual bool Fixed
        {
            get
            {
                return isFixed;
            }

            set
            {
                isFixed = value;

                if (isFixed)
                {
                    Acceleration = Vector.NULL;
                    velocity     = Vector.NULL;
                }
            }
        }

        /// <summary>
        /// The object's mass in kilograms.
        /// </summary>
        public double Mass
        {
            get
            {
                return mass;
            }

            set
            {
                if (value <= 0.0)
                {
                    throw new ArgumentOutOfRangeException();
                }

                mass = value;
            }
        }

        /// <summary>
        /// The object's current acceleration vector in m/s^2.
        /// </summary>
        public Vector Acceleration { get; private set; }

        /// <summary>
        /// The object's current velocity vector in m/s.
        /// Cannot be modified if the object is fixed.
        /// </summary>
        public Vector Velocity
        {
            get
            {
                return velocity;
            }

            set
            {
                if (isFixed)
                {
                    throw new FixedObjectException(this, "set Velocity");
                }

                velocity = value;
            }
        }

        /// <summary>
        /// The object's current location vector in m.
        /// </summary>
        public Vector Position { get; set; }

        /// <summary>
        /// The object's current scaling factors in (x, y, z) directions.
        /// </summary>
        public Vector Scaling { get; set; }

        /// <summary>
        /// The object's current rotation about the (x, y, z) axes in radians.
        /// </summary>
        public Vector Rotation { get; set; }

        /// <summary>
        /// Get the Centric Force Field of the object
        /// </summary>
        public CentricForceField ForceField { get; internal set; }

        /// <summary>
        /// Used for debug purposes only. ROAR!
        /// </summary>
        public bool Highlighted { get; set; }

        /// <summary>
        /// The object's ID.
        /// </summary>
        public int ID { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Updates the object's acceleration, velocity, and position by applying the given force
        /// over the given amount of time.
        /// </summary>
        /// <param name="force">The force to be applied to the object.</param>
        /// <param name="time">How long the force is applied (in seconds).</param>
        /// <exception cref="FixedObjectException">The object is fixed.</exception>
        public void ApplyForce(Vector force, double time)
        {
            if (isFixed)
            {
                throw new FixedObjectException(this, "UpdatePosition");
            }

            Vector newAcceleration = force / Mass;

            // Use modified Velocity Verlet algorithm to update position and velocity.

            Position += Velocity * time;

            if (!newAcceleration.IsNull())
            {
                Position += Acceleration / 2 * time * time;
            }

            if (!Acceleration.IsNull() && !newAcceleration.IsNull())
            {
                Velocity += (Acceleration + newAcceleration) / 2 * time;
            }

            if (Velocity.GetLengthSquare() < PhysicsEngine.MIN_VELOCITY_THRESHOLD)
            {
                Velocity = Vector.NULL;
            }

            // Update acceleration.
            Acceleration = newAcceleration;
        }

        /// <summary>
        /// Calculates and returns the object's transformation matrix from the current scaling,
        /// rotation, and position.
        /// </summary>
        /// <returns>The object's transformation matrix.</returns>
        public Matrix GetTransformation()
        {
            return Transformations.Scaling(Scaling)
                 * Transformations.Rotation(Rotation)
                 * Transformations.Translation(Position);
        }

        /// <summary>
        /// Translates the object by the given vector.
        /// </summary>
        /// <param name="v">The translation vector.</param>
        public virtual void Translate(Vector v)
        {
            Position += v;
        }

        /// <summary>
        /// Translates the object by (dx, dy, dz).
        /// </summary>
        /// <param name="dx">Translation amount in x-direction.</param>
        /// <param name="dy">Translation amount in y-direction.</param>
        /// <param name="dz">Translation amount in z-direction.</param>
        public virtual void Translate(double dx, double dy, double dz)
        {
            Position.X += dx;
            Position.Y += dy;
            Position.Z += dz;
        }

        /// <summary>
        /// Scales the object by (sx, sy, sz).
        /// </summary>
        /// <param name="sx">Scaling factor in x-direction.</param>
        /// <param name="sy">Scaling factor in y-direction.</param>
        /// <param name="sz">Scaling factor in z-direction.</param>
        public virtual void Scale(double sx, double sy, double sz)
        {
            Scaling.X *= sx;
            Scaling.Y *= sy;
            Scaling.Z *= sz;
        }

        /// <summary>
        /// Scales the object uniformly by s.
        /// </summary>
        /// <param name="s">Scaling factor in all directions.</param>
        public void Scale(double s)
        {
            Scale(s, s, s);
        }

        /// <summary>
        /// Sets the object's scaling to (sx, sy, sz).
        /// </summary>
        /// <param name="sx">Scaling factor in x-direction.</param>
        /// <param name="sy">Scaling factor in y-direction.</param>
        /// <param name="sz">Scaling factor in z-direction.</param>
        public virtual void SetScaling(double sx, double sy, double sz)
        {
            Scaling.X = sx;
            Scaling.Y = sy;
            Scaling.Z = sz;
        }

        /// <summary>
        /// Sets the object's scaling to (s, s, s).
        /// </summary>
        /// <param name="s">Scaling factor in all directions.</param>
        public void SetScaling(double s)
        {
            SetScaling(s, s, s);
        }

        /// <summary>
        /// Rotates the object by (rx, ry, rz).
        /// </summary>
        /// <param name="rx">The rotation angle about x-axis in radians.</param>
        /// <param name="ry">The rotation angle about y-axis in radians.</param>
        /// <param name="rz">The rotation angle about z-axis in radians.</param>
        public virtual void Rotate(double rx, double ry, double rz)
        {
            Rotation.X += rx;
            Rotation.Y += ry;
            Rotation.Z += rz;
        }

        /// <summary>
        /// Returns a string with the acceleration, velocity, and position of the object.
        /// </summary>
        /// <returns>Acceleration, velocity, and position of the object.</returns>
        public override string ToString()
        {
            return String.Format("a = {0}, v = {1}, p = {2}",
                Acceleration, velocity, Position);
        }
        #endregion
    }
}